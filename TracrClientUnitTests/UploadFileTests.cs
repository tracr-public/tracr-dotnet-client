using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System;
using System.Diagnostics;
using System.IO.Enumeration;
using System.Net;
using System.Numerics;
using System.Resources;
using System.Xml.Schema;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class UploadFileTests : TestBase
    {
        public UploadFileTests(ITestOutputHelper output) : base(output, InitMockService())
        { }


        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            // Setup a respond for Image Upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/images")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Image Upload Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/images")
                    .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );

            // Setup a respond for Scan file upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/scan")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Scan file Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/scan")
                    .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );

            // Setup a respond for Scan Report file upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/scan/reports")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Scan Report file Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/scan/reports")
                    .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );


            // Setup a respond for Plan file upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/plan")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Plan file upload Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/plan")
                    .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );

            // Setup a respond for Video upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/videos")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Video upload Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/videos")
                    .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );

            // Setup a respond for Supplementary file upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/supplementary_files")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Supplementary file upload Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/supplementary_files")
                    .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );

            // Setup a respond for Grading Certificate upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/grading_certificates")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Grading Certificate upload Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/grading_certificates")
                     .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );

            // Setup a respond for Plan Report file upload Success
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadsuccess/files/plan/reports")
                    .Respond("application/json", @"{
                                ""hash"": ""32a1c0b50e7724684a96f2661bf70f67a465fd479a2e34833aedd003958bbe9d""
                            }");

            // Setup a respond for Plan Report file upload Failure
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestuploadfail/files/plan/reports")
                    .Respond(
                        HttpStatusCode.NotFound,
                        "application/json",
                        @"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }"
                        );

            return mockHttp;
        }


        [Fact]
        public async Task AppendImageFileRoughSuccessAsync()
        {
            // Add a first image
            await _client.AppendImageFileAsync("ctestuploadsuccess", "./data/rough/demo_rough_1.jpg","image 1",ImageTypeEnum.Default);
        }
        
        [Fact]
        public async Task AppendImageFileNotFoundFailAsync()
        {   
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () => 
                await _client.AppendImageFileAsync("ctestuploadfail", "unknown_file.jpg","image 1",ImageTypeEnum.Default));

        
        }

        [Fact]
        public async Task AppendImageFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendImageFileAsync("ctestuploadfail", "./data/rough/demo_rough_1.jpg", "image 1", ImageTypeEnum.Default));
            
            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }

        [Fact]
        public async Task AppendScanFileRoughSuccessAsync()
        {
            // Append a scan file
            await _client.AppendScanFileAsync("ctestuploadsuccess", "./data/rough/demo_rough_1.stl", description: "scan 1", scanDeviceManufacturer: "Scansoft", scanDeviceModel: "Scan Master");
        }
        
        [Fact]
        public async Task AppendScanFileNotFoundFailAsync()
        {
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () =>
                await _client.AppendScanFileAsync("ctestuploadfail", "unknown_file.stl", description: "scan 1", scanDeviceManufacturer: "Scansoft", scanDeviceModel: "Scan Master"));
        }

        [Fact]
        public async Task AppendScanFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendScanFileAsync("ctestuploadfail", "./data/rough/demo_rough_1.stl", description: "scan 1",scanDeviceManufacturer:"Scansoft", scanDeviceModel: "Scan Master"));
            
            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }

        [Fact]
        public async Task AppendScanReportFileRoughSuccessAsync()
        {
            // Append a scan file
            await _client.AppendScanReportFileAsync("ctestuploadsuccess", "./data/polished/demo_polished_1.xml", description: "scan 1", scanDeviceManufacturer: "Scansoft", scanDeviceModel: "Scan Master");
        }

        [Fact]
        public async Task AppendScanReportFileNotFoundFailAsync()
        {
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () =>
                await _client.AppendScanReportFileAsync("ctestuploadfail", "unknown_file.xml", description: "scan 1", scanDeviceManufacturer: "Scansoft", scanDeviceModel: "Scan Master"));
        }

        [Fact]
        public async Task AppendScanReportFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendScanReportFileAsync("ctestuploadfail", "./data/polished/demo_polished_1.xml", description: "scan 1", scanDeviceManufacturer: "Scansoft", scanDeviceModel: "Scan Master"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }

        [Fact]
        public async Task AppendPlanFileRoughSuccessAsync()
        {
            // Append a plan file
            await _client.AppendPlanFileAsync("ctestuploadsuccess", "./data/rough/demo_rough_1.srn", "plan 1",
                planSoftwareManufacturer: "PlanSoft",
                planSoftwareName: "Plan Master",
                planSoftwareVersion: "1.1");
        }
        
        [Fact]
        public async Task AppendPlanFileNotFoundFailAsync()
        {   
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () => 
                await _client.AppendPlanFileAsync("ctestuploadfail","./unknown.srn",
                description:"plan 1",
                scanDeviceManufacturer:"PlanSoft",
                scanDeviceModel:"Plan Master",
                planSoftwareVersion:"1.1"));
        }

        [Fact]
        public async Task AppendPlanFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendPlanFileAsync("ctestuploadfail", "./data/rough/demo_rough_1.srn", 
                description: "plan 1", 
                scanDeviceManufacturer: "PlanSoft", 
                scanDeviceModel: "Plan Master", 
                planSoftwareVersion: "1.1"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }

        [Fact]
        public async Task AppendVideoFileRoughSuccessAsync()
        {   
            // Append a first video file
            await _client.AppendVideoFileAsync("ctestuploadsuccess", "./data/rough/demo_rough_1.mp4","video 1");

            // Append a second video file
            //await _client.AppendVideoFileAsync(diamond.DiamondId,"./data/rough/demo_rough_1.mp4","video 2");
        }
        
        [Fact]
        public async Task AppendVideoFileNotFoundFailAsync()
        {
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () => 
                await _client.AppendVideoFileAsync("ctestuploadfail","./unknown.mp4","video 1"));
        }

        [Fact]
        public async Task AppendVideoFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendVideoFileAsync("ctestuploadfail", "./data/rough/demo_rough_1.mp4", "video 1"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }

        [Fact]
        public async Task AppendSupplementaryFileRoughSuccessAsync()
        {
            // Append a first supplementary file
            await _client.AppendSupplementaryFileAsync(
                "ctestuploadsuccess",
                "./data/rough/demo_rough_scan_report_1.csv",
                "scan report");

            // Append a second supplementary file
            //await _client.AppendSupplementaryFileAsync(diamond.DiamondId,"./data/rough/demo_rough_scan_report_1.csv","scan report 2");
        }
        
        [Fact]
        public async Task AppendSupplementaryFileNotFoundFailAsync()
        {
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () => 
                await _client.AppendSupplementaryFileAsync("ctestuploadfail","./unknown.mp4","scan report"));
        }

        [Fact]
        public async Task AppendSupplementaryFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendSupplementaryFileAsync("ctestuploadfail", "./data/rough/demo_rough_scan_report_1.csv", "scan report"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }

        [Fact]
        public async Task AppendGradingCertificateFileRoughSuccessAsync()
        {
            // Append a Grading Certificate file
            await _client.AppendGradingCertificateAsync(
                "ctestuploadsuccess",
                "./data/polished/demo_polished_1.xml", "GIA",DateTime.Now,
                "123456");

        }

        [Fact]
        public async Task AppendGradingCertificateFileNotFoundFailAsync()
        {
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () =>
            await _client.AppendGradingCertificateAsync("ctestuploadfail", "./unknown.mp4", "GIA", DateTime.Now,
                "123456"));
        }

        [Fact]
        public async Task AppendGradingCertificateFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendGradingCertificateAsync("ctestuploadfail","./data/polished/demo_polished_1.xml", "GIA", DateTime.Now,"123456"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }
        [Fact]
        public async Task AppendPlanReportFileRoughSuccessAsync()
        {
            // Append a plan file
            await _client.AppendPlanReportAsync("ctestuploadsuccess", "./data/rough/demo_rough_1.srn", "plan 1",
                planSoftwareManufacturer: "PlanSoft",
                planSoftwareName: "Plan Master",
                planSoftwareVersion: "1.1");
        }

        [Fact]
        public async Task AppendPlanReportFileNotFoundFailAsync()
        {
            // Upload an unknown file
            await Assert.ThrowsAsync<FileNotFoundException>(async () =>
                await _client.AppendPlanReportAsync("ctestuploadfail", "./unknown.srn",
                description: "plan 1",
                scanDeviceManufacturer: "PlanSoft",
                scanDeviceModel: "Plan Master",
                planSoftwareVersion: "1.1"));
        }

        [Fact]
        public async Task AppendPlanReportFileUnknownDiamondFailAsync()
        {
            // Append a file on an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.AppendPlanReportAsync("ctestuploadfail", "./data/rough/demo_rough_1.srn",
                description: "plan 1",
                scanDeviceManufacturer: "PlanSoft",
                scanDeviceModel: "Plan Master",
                planSoftwareVersion: "1.1"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestuploadfail could not be found.""
                          }", ex.Message);
        }
    }
}
