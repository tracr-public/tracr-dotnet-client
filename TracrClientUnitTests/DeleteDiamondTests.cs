using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Net;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class DeleteDiamondTests : TestBase
    {
        public DeleteDiamondTests(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            // Create Rough diamond

            var deleteDiamondAnswer = new DeleteDiamondResponse()
            {
                Results= new List<DeleteDiamondResponseItem>()
                {
                    new DeleteDiamondResponseItem()
                    {
                        DiamondId = "ctestdeletesuccess",
                        Descendants = new List<DeleteDiamondResponseDescendant>(),
                        Error = null,
                        ParticipantId = "testparticipant",
                        Status = DeleteStatusEnum.Success
                    }
                }
            };

            // Setup a successfull response for destroying a rough diamond
            mockHttp.When(HttpMethod.Post, "http://localhost/bulk/diamonds/delete")
                .Respond("application/json", JsonConvert.SerializeObject(deleteDiamondAnswer)); // Respond with JSONs

            return mockHttp;
        }

        [Fact]
        public async Task DestroyRoughDiamondSuccessAsync()
        {
            var diamondsToDelete = new List<DeleteDiamondRequestItem>()
            {
                new DeleteDiamondRequestItem()
                {
                    DiamondId = "ctestdeletesuccess",
                    DeleteDescendants = true
                }
            };

            var deletionResult = await _client.DeleteDiamondAsync(diamondsToDelete);

            if (deletionResult.Results == null)
            {
                Assert.Fail("Results is null");
            }

            Assert.Single(deletionResult.Results);
            Assert.Equal(DeleteStatusEnum.Success, deletionResult.Results[0].Status);
        }
    }
}
