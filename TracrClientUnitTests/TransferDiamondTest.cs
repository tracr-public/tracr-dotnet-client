﻿using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Diagnostics;
using System.Net;
using System.Xml.Schema;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class TransferInAcceptTest : TestBase
    {
        public TransferInAcceptTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            List<TransferInAcceptResponseItem> diamondsIdVal = new List<TransferInAcceptResponseItem>();

            diamondsIdVal.Add(new TransferInAcceptResponseItem
            {
                DiamondId = "TransferInAcceptTestId1",
                InscriptionNumber = "GIA 123456789_1",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferInAcceptResponseItem
            {
                DiamondId = "TransferInAcceptTestId2",
                InscriptionNumber = "GIA 123456789_2",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferInAcceptResponseItem
            {
                DiamondId = "TransferInAcceptTestId3",
                InscriptionNumber = "GIA 123456789_3",
                Status = TransferStatusEnum.Success
            });

            // Setup a single success response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/in/ctestshipmentsuccess/accept")
                  .Respond("application/json", JsonConvert.SerializeObject(diamondsIdVal)); // Respond with JSONs

            // Setup a failure response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/in/ctestshipmentfailure/accept")
                  .Respond(HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }"
            ); // Respond with JSON

            return mockHttp;
        }

        
        [Fact]
        public async Task TransferInAcceptSuccess()
        {
            List<TransferInAcceptItem> diamondIdsAccept = new List<TransferInAcceptItem>();
            diamondIdsAccept.Add(new TransferInAcceptItem
            {
                DiamondId = "clg263d5100010au8iax5v2d6",
                InscriptionNumber = "30547880101"
            }
            );

            List<TransferInAcceptResponseItem> transferInAcceptDiamond = await _client.TransferInAcceptAsync("ctestshipmentsuccess", diamondIdsAccept);
        }

        [Fact]
        public async Task TransferInAcceptUnknownIdAsync()
        {
            List<TransferInAcceptItem> diamondIdsAccept = new List<TransferInAcceptItem>();
            diamondIdsAccept.Add(new TransferInAcceptItem
            {
                DiamondId = "clg263d5100010au8iax5v2d6",
                InscriptionNumber = "30547880101"
            }
            );

            // Unable to retrieve shipment details
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.TransferInAcceptAsync("ctestshipmentfailure", diamondIdsAccept));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }", ex.Message);
        }

    }

    public class TransferInRejectTest : TestBase
    {

        public TransferInRejectTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            List<TransferInRejectResponseItem> diamondsIdVal = new List<TransferInRejectResponseItem>();

            diamondsIdVal.Add(new TransferInRejectResponseItem
            {
                DiamondId = "TransferInRejectTestId1",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferInRejectResponseItem
            {
                DiamondId = "TransferInRejectTestId2",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferInRejectResponseItem
            {
                DiamondId = "TransferInRejectTestId3",
                Status = TransferStatusEnum.Success
            });


            // Setup a single success response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/in/ctestshipmentsuccess/reject")
                  .Respond("application/json", JsonConvert.SerializeObject(diamondsIdVal)); // Respond with JSONs

            // Setup a failure response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/in/ctestshipmentfailure/reject")
                  .Respond(HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }"
            ); // Respond with JSON

            return mockHttp;
        }


        [Fact]
        public async Task TransferInRejectSuccess()
        {
            List<TransferInRejectItem> diamondIdsReject = new List<TransferInRejectItem>();
            diamondIdsReject.Add(new TransferInRejectItem
            {
                DiamondId = "clivgpuoj000008uz4v8ek4uc"
            }
            );

            List<TransferInRejectResponseItem> transferInRejectDiamond = await _client.TransferInRejectAsync("ctestshipmentsuccess", diamondIdsReject);
        }

        [Fact]
        public async Task TransferInRejectUnknownIdAsync()
        {
            List<TransferInRejectItem> diamondIdsReject = new List<TransferInRejectItem>();
            diamondIdsReject.Add(new TransferInRejectItem
            {
                DiamondId = "clivgpuoj000008uz4v8ek4uc"
            }
            );

            // Unable to retrieve shipment details
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.TransferInRejectAsync("ctestshipmentfailure", diamondIdsReject));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }", ex.Message);
        }
    }

    public class TransferOutStartTest : TestBase
    {

        public TransferOutStartTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            List<TransferOutStartResponseItem> diamondsIdVal = new List<TransferOutStartResponseItem>();

            diamondsIdVal.Add(new TransferOutStartResponseItem 
            {
                DiamondId = "TransferOutStartTestId1",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferOutStartResponseItem
            {
                DiamondId = "TransferOutStartTestId2",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferOutStartResponseItem
            {
                DiamondId = "TransferOutStartTestId3",
                Status = TransferStatusEnum.Success
            });

            var createTransferOutStartAnswer = new TransferOutStartResponse()
            {
                ShipmentId = "ctestshipmentsuccess",
                ShipmentName = "cctestshipmentname",
                Diamonds = diamondsIdVal
            };

            // Setup a success response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/out/start")
                  .Respond("application/json", JsonConvert.SerializeObject(createTransferOutStartAnswer)); // Respond with JSONs

            return mockHttp;
        }


        [Fact]
        public async Task TransferOutStartSuccess()
        {
            List<string> diamondIds = new List<string>();
            diamondIds.Add("TransferOutStartTestId1");
            diamondIds.Add("TransferOutStartTestId2");
            diamondIds.Add("TransferOutStartTestId3");
            string receiver = "https://api.2d2a6c26-2cfd-4213-a646-68ecc15b5f7a.uat.tracr.com";

            TransferOutStartResponse transfer = await _client.TransferOutStartAsync("cctestshipmentname", receiver, diamondIds);
        }

    }

    public class TransferOutStartUnknownTest : TestBase
    {

        public TransferOutStartUnknownTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            // Setup a failure response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/out/start")
                  .Respond(HttpStatusCode.BadRequest,
                    "application/json",
                    @"{
                            ""message"": ""Unable to create shipment."",
                            ""diamonds"": [
                                {
                                    ""diamond_id"": ""ctransferoutstarttestId1"",
                                    ""status"": ""diamond_does_not_exist""
                                }
                            ]
                        }"
            ); // Respond with JSON

            return mockHttp;
        }


        [Fact]
        public async Task TransferOutStartUnknownIdAsync()
        {
            List<string> diamondIds = new List<string>();
            diamondIds.Add("ctransferoutstarttestId1");
            
            string receiver = "https://api.2d2a6c26-2cfd-4213-a646-68ecc15b5f7a.uat.tracr.com";

            // Unable to retrieve shipment details
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.TransferOutStartAsync("cctestunknowndiamond", receiver, diamondIds));

            Assert.Equal(HttpStatusCode.BadRequest, ex.StatusCode);
            Assert.Equal(@"{
                            ""message"": ""Unable to create shipment."",
                            ""diamonds"": [
                                {
                                    ""diamond_id"": ""ctransferoutstarttestId1"",
                                    ""status"": ""diamond_does_not_exist""
                                }
                            ]
                        }", ex.Message);
        }
    }

    public class TransferOutCancelTest : TestBase
    {

        public TransferOutCancelTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            List<TransferOutCancelResponseItem> diamondsIdVal = new List<TransferOutCancelResponseItem>();

            diamondsIdVal.Add(new TransferOutCancelResponseItem
            {
                DiamondId = "TransferOutCancelTestId1",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferOutCancelResponseItem
            {
                DiamondId = "TransferOutCancelTestId2",
                Status = TransferStatusEnum.Success
            });
            diamondsIdVal.Add(new TransferOutCancelResponseItem
            {
                DiamondId = "TransferOutCancelTestId3",
                Status = TransferStatusEnum.Success
            });


            // Setup a single success response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/out/ctestshipmentsuccess/cancel")
                  .Respond("application/json", JsonConvert.SerializeObject(diamondsIdVal)); // Respond with JSONs

            // Setup a failure response
            mockHttp.When(HttpMethod.Post, "http://localhost/transfer/out/ctestshipmentfailure/cancel")
                  .Respond(HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }"
            ); // Respond with JSON

            return mockHttp;
        }


        [Fact]
        public async Task TransferOutCancelSuccess()
        {
            List<TransferOutCancelItem> diamondIdsCancel = new List<TransferOutCancelItem>();
            diamondIdsCancel.Add(new TransferOutCancelItem
            {
                DiamondId = "clitwh0j60004lzytlid7ejfj"
            }
            );

            List<TransferOutCancelResponseItem> transferOutCancelDiamond = await _client.TransferOutCancelAsync("ctestshipmentsuccess", diamondIdsCancel);
        }

        [Fact]
        public async Task TransferOutCancelUnknownIdAsync()
        {
            List<TransferOutCancelItem> diamondIdsCancel = new List<TransferOutCancelItem>();
            diamondIdsCancel.Add(new TransferOutCancelItem
            {
                DiamondId = "clitwh0j60004lzytlid7ejfj"
            }
            );

            // Unable to retrieve shipment details
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.TransferOutCancelAsync("ctestshipmentfailure", diamondIdsCancel));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }", ex.Message);
        }
    }
}