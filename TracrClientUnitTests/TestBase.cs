using TracrClient;
using Xunit.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using TracrClient.Extensions;

namespace TracrClientTests
{
    public class TestBase : IDisposable
    {
        protected TracrService? _client;
        private IServiceProvider? _serviceProvider;
        protected ITestOutputHelper _output;

        protected TestBase(ITestOutputHelper output, HttpMessageHandler messageHandler)
        {
            var services = new ServiceCollection();

            services.AddLogging();

            services.AddTracrClient(new TracrClientOptions()
            {
                ApiUrl= "http://localhost",
                TokenUrl = "http://localhost/token",
                ClientId = "id",
                ClientSecret = "secret",
            }, messageHandler);

            _serviceProvider = services.BuildServiceProvider();

            _client = _serviceProvider?.GetService<TracrService>();
            _output = output;
        }
        
        public void Dispose()
        {
            _client?.Dispose();
            
            switch (_serviceProvider)
            {
                case null:
                    return;
                case IDisposable disposable:
                    disposable.Dispose();
                    break;
            }
        }
    }
}