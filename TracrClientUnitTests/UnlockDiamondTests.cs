using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Net;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class UnlockDiamondTests : TestBase
    {
        public UnlockDiamondTests(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            // Create Rough diamond

            var unlockDiamondAnswer = new UnlockDiamondResponse()
            {
                Diamonds = new List<UnlockDiamondResponseItem>()
                {
                    new UnlockDiamondResponseItem()
                    {
                        DiamondId = "ctestunlocksuccess",
                        Status = UnlockStatusEnum.Success
                    }
                }
            };

            // Setup a successfull response for destroying a rough diamond
            mockHttp.When(HttpMethod.Post, "http://localhost/bulk/diamonds/unlock")
                .Respond("application/json", JsonConvert.SerializeObject(unlockDiamondAnswer)); // Respond with JSONs

            return mockHttp;
        }

        [Fact]
        public async Task UnlockPolishedDiamondSuccessAsync()
        {
            var diamondsToUnlock = new List<UnlockDiamondRequestItem>()
            {
                new UnlockDiamondRequestItem()
                {
                    DiamondId = "ctestunlocksuccess"
                }
            };

            var unlockResult = await _client.UnlockDiamondAsync(diamondsToUnlock);

            if(unlockResult.Diamonds == null)
            {
                Assert.Fail("Diamonds is null");
            }
            
            Assert.Single(unlockResult.Diamonds);
            Assert.Equal(UnlockStatusEnum.Success, unlockResult.Diamonds[0].Status);
        }
    }
}
