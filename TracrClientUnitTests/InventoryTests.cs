﻿using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Net;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class InventoryTests : TestBase
    {
        public InventoryTests(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            // Create Rough diamond

            var roughDiamondAnswer = new Diamond()
            {
                ParticipantTimestamp = new DateTime(),
                ParticipantId="testid",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfggetdiamond",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestretreiveroughdiamondsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            // Setup a respond
            mockHttp.When(HttpMethod.Get, "http://localhost/diamonds/ctestretreiveroughdiamondsuccess")
                  .Respond("application/json", JsonConvert.SerializeObject(roughDiamondAnswer)); // Respond with JSONs

            // Setup a Not Found response when rough id doesn't exists
            mockHttp.When(HttpMethod.Get, "http://localhost/diamonds/ctestretreiveroughdiamondunknown")
                .Respond(HttpStatusCode.NotFound,
                    "application/json",
                    @"{
                        ""message"": ""Diamond ctestretreiveroughdiamondunknown could not be found.""
                    }"
            );

            return mockHttp;
        }

        [Fact]
        public async Task GetSingleRoughDiamondSuccessAsync()
        {
            // Retrieve a Rough Diamond from the inventory
            var roughDiamond = await _client.GetDiamondAsync("ctestretreiveroughdiamondsuccess");

            Assert.Equal(LifecycleStateEnum.Rough, roughDiamond.LifecycleState);
            Assert.Equal(PossessionStateEnum.Held, roughDiamond.PossessionState);
            Assert.Equal("ctestretreiveroughdiamondsuccess", roughDiamond.DiamondId);
            Assert.Equal("cl1234156789asdfggetdiamond", roughDiamond.VersionId);
        }

        [Fact]
        public async Task GetSingleRoughDiamondUnknownIdAsync()
        {
            // Unable to retrieve a Rough Diamond from the inventory
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.GetDiamondAsync("ctestretreiveroughdiamondunknown"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Diamond ctestretreiveroughdiamondunknown could not be found.""
                    }", ex.Message);
        }
    }
}
