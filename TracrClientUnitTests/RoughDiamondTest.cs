using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Diagnostics;
using System.Net;
using System.Xml.Schema;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class CreateRoughDiamondsSuccess : TestBase
    {
        public CreateRoughDiamondsSuccess(ITestOutputHelper output) : base(output, InitMockService())
        {}

        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            var createDiamondAnswer = new Diamond()
            {
                ParticipantTimestamp = new DateTime(),
                ParticipantId="testid",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "cl1234156789asdfg",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "All good",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            // Setup a respond
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/rough/create")
                  .Respond("application/json", JsonConvert.SerializeObject(createDiamondAnswer)); // Respond with JSONs

            return mockHttp;
        }

        [Fact]
        public async Task CreateRoughDiamondSuccess()
        {
            var newRoughDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId= "testid",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Rough = new Rough
                {
                    Carats = 7.500,
                }
            };

            Diamond diamond = await _client?.CreateRoughDiamondAsync(newRoughDiamond);

            Assert.IsType<Diamond>(diamond);
            Assert.Equal(VerificationStatusEnum.Verified, diamond.VerificationStatus);
            Assert.Equal(LifecycleStateEnum.Rough, diamond.LifecycleState);
            Assert.Equal(PossessionStateEnum.Held, diamond.PossessionState);
        }
    }

    public class CreateRoughDiamondsFailureUnprocessableEntity : TestBase
    {
        public CreateRoughDiamondsFailureUnprocessableEntity(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            // Setup a respond
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/rough/create")
            .Respond(
                HttpStatusCode.UnprocessableEntity, 
                "application/json",
                @"{
                    ""message"": [
                        {
                            ""loc"": [
                                ""body"",
                                ""rough"",
                                ""carats""
                            ],
                            ""msg"": ""field required"",
                            ""type"": ""value_error.missing""
                        }
                    ]
                }"
            ); // Respond with JSONs

            return mockHttp;
        }

        [Fact]
        public async Task CreateRoughDiamondFailUnprocessableEntityAsync()
        {

            var newRoughDiamond = new Diamond()
            {
                ParticipantTimestamp = new DateTime(),
                ParticipantId= "testid",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Rough = new Rough()
            };

            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.CreateRoughDiamondAsync(newRoughDiamond));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                    ""message"": [
                        {
                            ""loc"": [
                                ""body"",
                                ""rough"",
                                ""carats""
                            ],
                            ""msg"": ""field required"",
                            ""type"": ""value_error.missing""
                        }
                    ]
                }", ex.Message);
        }
    }

    public class UpdateRoughDiamondTests : TestBase
    {
        public UpdateRoughDiamondTests(ITestOutputHelper output) : base(output, InitMockService())
        { }


        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            var updateRoughDiamondAnswer = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testid",
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 7.500
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfgupdate",
                SystemTimestamp = DateTime.Now,
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdateroughsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "All good",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            // Rough Diamond Update Success
            mockHttp.When(HttpMethod.Patch, "http://localhost/diamonds/ctestupdateroughsuccess/rough")
                .Respond("application/json", JsonConvert.SerializeObject(updateRoughDiamondAnswer)); // Respond with JSONs

            // Rough Diamond Not Found
            mockHttp.When(HttpMethod.Patch, "http://localhost/diamonds/ctestupdateroughunknown/rough")
                .Respond(HttpStatusCode.NotFound,
                    "application/json",
                    @"{
                        ""message"": ""Diamond ctestupdateroughunknown could not be found.""
                    }"); // Respond with JSONs

            // Payload incomplete
            mockHttp.When(HttpMethod.Patch, "http://localhost/diamonds/ctestupdateroughincomplete/rough")
                .Respond(
                HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }"); // Respond with JSONs


            return mockHttp;
        }


        [Fact]
        public async Task UpdateRoughDiamondSuccessAsync()
        {
            // Update a Rough Diamond
            var roughDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testid",
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 7.500
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = DateTime.Now,
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdateroughsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "All good",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            var updatedDiamond = await _client.UpdateRoughDiamondAsync(roughDiamond);

            Assert.IsType<Diamond>(updatedDiamond);
            Assert.Equal("cl1234156789asdfgupdate", updatedDiamond.VersionId);
        }

        [Fact]
        public async Task UpdateRoughDiamondFailUnknownAsync()
        {
            // Update a Rough Diamond
            var roughDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testid",
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 7.500
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = DateTime.Now,
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdateroughunknown",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "All good",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.UpdateRoughDiamondAsync(roughDiamond));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Diamond ctestupdateroughunknown could not be found.""
                    }", ex.Message);
        }

        [Fact]
        public async Task UpdateRoughDiamondFailIncompleteAsync()
        {
            // Update a Rough Diamond
            var roughDiamond = new Diamond()
            {
                ParticipantId = "testid",
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 7.500,
                    Quality = "Good"
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = DateTime.Now,
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdateroughincomplete",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "All good",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.UpdateRoughDiamondAsync(roughDiamond));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }", ex.Message);
        }
    }

    public class SplitRoughDiamondTests : TestBase
    {
        public SplitRoughDiamondTests(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            var splitRoughDiamondAnswer = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testid",
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 7.500
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfgsplit",
                SystemTimestamp = DateTime.Now,
                LifecycleState = LifecycleStateEnum.Split,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Destroyed,
                DiamondId = "ctestroughsplitsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            // Setup a successful respond for splitting a rough diamond
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestroughsplitsuccess/split")
                .Respond("application/json", JsonConvert.SerializeObject(splitRoughDiamondAnswer));

            // Setup a respond for failure of splitting a rough diamond
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestunknownroughsplit/split")
                .Respond(HttpStatusCode.NotFound,
                    "application/json",
                    @"{
                        ""message"": ""Diamond ctestunknownroughsplit could not be found.""
                    }");

            // Setup a respond for failure of Child Diamond creation
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestroughsplitincomplete/split")
                .Respond(
                    HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }");

            return mockHttp;
        }

        [Fact]
        public async Task SplitRoughDiamondSuccessAsync()
        {
            var roughDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testid",
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 7.500
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = DateTime.Now,
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestroughsplitsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            var splitDiamond = await _client.SplitDiamondAsync(roughDiamond.DiamondId, DateTime.Now);

            Assert.IsType<Diamond>(splitDiamond);
            Assert.Equal(LifecycleStateEnum.Split, splitDiamond.LifecycleState);
            Assert.Equal(PossessionStateEnum.Destroyed, splitDiamond.PossessionState);
            Assert.Equal("cl1234156789asdfgsplit", splitDiamond.VersionId);
        }

        [Fact]
        public async Task SplitUnknownRoughDiamondAsync()
        {
            // Split an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                    await _client.SplitDiamondAsync("ctestunknownroughsplit", DateTime.Now));
            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Diamond ctestunknownroughsplit could not be found.""
                    }", ex.Message);
        }


        [Fact]
        public async Task SplitRoughDiamondIncompleteAsync()
        {
            // Split a rough diamond with an incomplete request
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                    await _client.SplitDiamondAsync("ctestroughsplitincomplete", DateTime.Now));
            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }", ex.Message);

        }
    }

    public class CreateChildRoughDiamondTests : TestBase
    {
        public CreateChildRoughDiamondTests(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            var createRoughChildAnswer = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testid",
                ParticipantParentId = "testparentid",
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 7.500
                },
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfgchild",
                SystemTimestamp = DateTime.Now,
                LifecycleState = LifecycleStateEnum.Rough,
                CurrentStage = "rough",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestchildroughsuccess",
                DiamondParentId = "ctestparentroughsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                }
            };

            // Setup a successfull response for creating a child diamond
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestparentroughsuccess/rough/create")
                .Respond("application/json", JsonConvert.SerializeObject(createRoughChildAnswer));

            // Setup a Not Found response when rough parent id doesn't exists
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestunknownparent/rough/create")
                .Respond(HttpStatusCode.NotFound,
                    "application/json",
                    @"{
                                ""message"": ""Diamond ctestunknownparent could not be found.""
                          }");

            // Setup a Unprocessable Entity error when passing an incomplete payload
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestincompletepayload/rough/create")
                .Respond(
                    HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }");

            // Setup a Unprocessable Entity error when sum of all child carats > parent carats
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctesttoomanycarats/rough/create")
                .Respond(
                    HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": ""Failed to create child diamond(7.500cts) for parent ctesttoomanycarats (5.000cts) with 5.000cts filled.""
                    }");

            

            return mockHttp;
        }

        [Fact]
        public async Task CreateChildRoughDiamondSuccessAsync()
        {
            var childDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testchildid",
                Rough = new Rough
                {
                    Carats = 7.500
                }
            };

            childDiamond = await _client.CreateRoughChildDiamondAsync("ctestparentroughsuccess", childDiamond);

            Assert.IsType<Diamond>(childDiamond);
            Assert.Equal(LifecycleStateEnum.Rough, childDiamond.LifecycleState);
            Assert.Equal(PossessionStateEnum.Held, childDiamond.PossessionState);
            Assert.Equal("cl1234156789asdfgchild", childDiamond.VersionId);
            Assert.Equal("ctestparentroughsuccess", childDiamond.DiamondParentId);
            Assert.Equal("DTC", childDiamond.Provenance.ProvenanceName);
        }

        [Fact]
        public async Task CreateChildRoughDiamondUnknownParentAsync()
        {
            var childDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testchildid",
                Rough = new Rough
                {
                    Carats = 7.500
                }
            };

            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                    await _client.CreateRoughChildDiamondAsync("ctestunknownparent", childDiamond));
            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                                ""message"": ""Diamond ctestunknownparent could not be found.""
                          }", ex.Message);
        }


        [Fact]
        public async Task CreateChildRoughDiamondIncompletePayloadAsync()
        {
            var childDiamond = new Diamond()
            {
                ParticipantId = "testchildid",
                Rough = new Rough
                {
                    Carats = 7.500
                }
            };

            // Split an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                    await _client.CreateRoughChildDiamondAsync("ctestincompletepayload", childDiamond));
            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }", ex.Message);

        }

        [Fact]
        public async Task CreateChildRoughDiamondTooManyCaratsAsync()
        {
            var childDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId = "testchildid",
                Rough = new Rough
                {
                    Carats = 7.500
                }
            };

            // Split an unknown diamond
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                    await _client.CreateRoughChildDiamondAsync("ctesttoomanycarats", childDiamond));
            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Failed to create child diamond(7.500cts) for parent ctesttoomanycarats (5.000cts) with 5.000cts filled.""
                    }", ex.Message);

        }
    }
}


