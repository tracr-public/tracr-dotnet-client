﻿using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Moq;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Xml.Schema;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class SingleInboundShipmentTest : TestBase
    {
        public SingleInboundShipmentTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            var createShipmentAnswer = new InboundShipment()
            {
                Id = "shipmenttestsuccess",
                Name = "TRACR",
                Sender = "https://test.local",
                CreatedAt = DateTime.Now,
                Diamonds = new List<ShipmentDiamond>(){
                    new ShipmentDiamond()
                    {
                        Carats = 2.5,
                        Clarity = ClarityEnum.FL,
                        Colour = ColourEnum.D,
                        CutGrade = CutGradeEnum.EX,
                        Shape = ShapeEnum.HalfMoon,
                        TransferState = TransferEnum.Accepted,
                        DiamondId = "ctestshipmentsuccess",
                        UpdatedAt = new DateTime()
                    }
                }
            };

            // Setup a single success response
            mockHttp.When(HttpMethod.Get, "http://localhost/shipments/inbound/shipmenttestsuccess")
                  .Respond("application/json", JsonConvert.SerializeObject(createShipmentAnswer)); // Respond with JSONs

            // Setup a failure response
            mockHttp.When(HttpMethod.Get, "http://localhost/shipments/inbound/shipmenttestunknown")
                  .Respond(HttpStatusCode.NotFound,
                    "application/json",
                    @"{
                        ""message"": ""The shipment you requested does not exist.""
                    }"
            ); // Respond with JSON

            return mockHttp;
        }

        [Fact]
        public async Task GetSingleInboundShipmentSuccess()
        {
            //Get the shipment details for 1 shipment id
            InboundShipment shipment = await _client.GetInboundShipmentAsync("shipmenttestsuccess");
        }

        [Fact]
        public async Task GetSingleInboundShipmentUnknownIdAsync()
        {
            // Unable to retrieve shipment details
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.GetInboundShipmentAsync("shipmenttestunknown"));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""The shipment you requested does not exist.""
                    }", ex.Message);
        }
    }

    public class MultipleInboundShipmentTest : TestBase
    {
        public MultipleInboundShipmentTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            List<InboundShipment> shipmentList = new List<InboundShipment>();

            shipmentList.Add(new InboundShipment
            {
                Id = "clhhqyev2000ykcj1fo306tk1",
                Name = "TRACR1",
                Sender = "https://test.local",
                CreatedAt = DateTime.Now,
                Diamonds = new List<ShipmentDiamond>()
                {
                    new ShipmentDiamond(){
                        Carats = 2.5,
                        Clarity = ClarityEnum.FL,
                        Colour = ColourEnum.D,
                        CutGrade = CutGradeEnum.EX,
                        Shape = ShapeEnum.HalfMoon,
                        TransferState = TransferEnum.Accepted,
                        DiamondId = "TestShipmentSuccess1",
                        UpdatedAt = DateTime.Now
                    }
                }
            });

            shipmentList.Add(new InboundShipment
            {
                Id = "clhhqyev2000ykcj1fo306tk2",
                Name = "TRACR2",
                Sender = "https://test.local",
                CreatedAt = DateTime.Now,
                Diamonds = new List<ShipmentDiamond>()
                {
                    new ShipmentDiamond(){
                        Carats = 2.5,
                        Clarity = ClarityEnum.FL,
                        Colour = ColourEnum.D,
                        CutGrade = CutGradeEnum.EX,
                        Shape = ShapeEnum.HalfMoon,
                        TransferState = TransferEnum.Accepted,
                        DiamondId = "TestShipmentSuccess2",
                        UpdatedAt = DateTime.Now
                    }
                }
            });

            var result = new AllInboundShipments { Items = shipmentList };


            // Setup a multiple success response
            mockHttp.When(HttpMethod.Get, "http://localhost/shipments/inbound")
                  .Respond("application/json", JsonConvert.SerializeObject(result)); // Respond with JSONs

            return mockHttp;
        }

        [Fact]
        public async Task GetMultipleInboundShipmentSuccess()
        {
            //Get the shipment details for multiple shipment id
            List<InboundShipment> shipments = await _client.GetAllInboundShipmentsAsync();
            Assert.NotEmpty(shipments);
        }
    }

    public class SingleOutboundShipmentTest : TestBase
    {
        public SingleOutboundShipmentTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            var createShipmentAnswer = new OutboundShipment()
            {
                Id = "clhhqyev2000ykcj1fo306tk3",
                Name = "TRACR",
                Receiver = "https://test.local",
                CreatedAt = new DateTime(),
                Diamonds = new List<ShipmentDiamond>()
                {
                    new ShipmentDiamond(){
                        Carats = 2.5,
                        Clarity = ClarityEnum.FL,
                        Colour = ColourEnum.D,
                        CutGrade = CutGradeEnum.EX,
                        Shape = ShapeEnum.HalfMoon,
                        TransferState = TransferEnum.Accepted,
                        DiamondId = "TestShipmentSuccess1",
                        UpdatedAt = DateTime.Now
                    }
                }
            };

            // Setup a single success response
            mockHttp.When(HttpMethod.Get, "http://localhost/shipments/outbound/ctestshipmentsuccess")
                  .Respond("application/json", JsonConvert.SerializeObject(createShipmentAnswer)); // Respond with JSONs

            // Setup a failure response
            mockHttp.When(HttpMethod.Get, "http://localhost/shipments/outbound/ctestshipmentfailure")
                  .Respond(HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }"
            ); // Respond with JSON

            return mockHttp;
        }

        [Fact]
        public async Task GetSingleOutboundShipmentSuccess()
        {
            //Get the shipment details for 1 shipment id
            OutboundShipment shipments = await _client.GetOutboundShipmentAsync("ctestshipmentsuccess");
        }

        [Fact]
        public async Task GetSingleOutboundShipmentUnknownIdAsync()
        {
            // Unable to retrieve shipment details
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.GetOutboundShipmentAsync("ctestshipmentfailure"));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Shipment ctestshipmentfailure could not be processed.""
                    }", ex.Message);
        }
    }

    public class MultipleOutboundShipmentTest : TestBase
    {
        public MultipleOutboundShipmentTest(ITestOutputHelper output) : base(output, InitMockService())
        { }

        private static HttpMessageHandler InitMockService()
        {
            var mockHttp = new MockHttpMessageHandler();

            List<OutboundShipment> shipmentList = new List<OutboundShipment>();


            shipmentList.Add(new OutboundShipment
            {
                Id = "clhhqyev2000ykcj1fo306tk1",
                Name = "TRACR1",
                Receiver = "https://test.local",
                CreatedAt = new DateTime(),
                Diamonds = new List<ShipmentDiamond>()
                {
                    new ShipmentDiamond(){
                        Carats = 2.5,
                        Clarity = ClarityEnum.FL,
                        Colour = ColourEnum.D,
                        CutGrade = CutGradeEnum.EX,
                        Shape = ShapeEnum.HalfMoon,
                        TransferState = TransferEnum.Accepted,
                        DiamondId = "TestShipmentSuccess1",
                        UpdatedAt = DateTime.Now
                    }
                }
            });

            shipmentList.Add(new OutboundShipment
            {
                Id = "clhhqyev2000ykcj1fo306tk2",
                Name = "TRACR2",
                Receiver = "https://test.local",
                CreatedAt = new DateTime(),
                Diamonds = new List<ShipmentDiamond>()
                {
                    new ShipmentDiamond(){
                        Carats = 2.5,
                        Clarity = ClarityEnum.FL,
                        Colour = ColourEnum.D,
                        CutGrade = CutGradeEnum.EX,
                        Shape = ShapeEnum.HalfMoon,
                        TransferState = TransferEnum.Accepted,
                        DiamondId = "TestShipmentSuccess1",
                        UpdatedAt = DateTime.Now
                    }
                }
            });

            var result = new AllOutboundShipments { Items = shipmentList };

            // Setup a multiple success response
            mockHttp.When(HttpMethod.Get, "http://localhost/shipments/outbound")
                  .Respond("application/json", JsonConvert.SerializeObject(result)); // Respond with JSONs

            return mockHttp;
        }

        [Fact]
        public async Task GetMultipleOutboundShipmentSuccess()
        {
            //Get the shipment details for multiple shipment id
            List<OutboundShipment> shipments = await _client.GetAllOutboundShipmentsAsync();
            Assert.NotEmpty(shipments);
        }
    }
}