using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Diagnostics;
using System.Net;
using System.Xml.Schema;
using TracrClient.Extensions;
using TracrClient.Models;
using TracrClientTests;
using Xunit.Abstractions;

namespace TracrClientUnitTests
{
    public class CreatePolishedDiamondTests : TestBase
    {
        public CreatePolishedDiamondTests(ITestOutputHelper output) : base(output, InitMockService())
        {}


        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            var polishDiamondAnswer = new Diamond()
            {
                ParticipantTimestamp = new DateTime(),
                ParticipantId="testid",
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfgpolished",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Polished,
                CurrentStage = "polished",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestpolishsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                },
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };

            // Setup a successfull response for polishing a rough diamond
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestpolishsuccess/polish")
                .Respond("application/json", JsonConvert.SerializeObject(polishDiamondAnswer)); // Respond with JSONs

            // Setup a Not Found response when rough diamond id doesn't exists
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestpolishunknownrough/polish")
                .Respond(HttpStatusCode.NotFound,
                    "application/json",
                    @"{
                        ""message"": ""Diamond ctestpolishunknownrough could not be found.""
                    }"
            );

            // Setup a Unprocessable Entity error when passing an incomplete payload
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctestincompletepayload/polish")
                .Respond(
                    HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }");

            // Setup a Unprocessable Entity error when polished carats > rough carats
            mockHttp.When(HttpMethod.Post, "http://localhost/diamonds/ctesttoomanycarats/polish")
                .Respond(
                    HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": ""Polished diamond cannot be heavier than its rough counterpart""
                    }");

            return mockHttp;
        }


        [Fact]
        public async Task PolishRoughDiamondSuccessAsync()
        { 
            var polishedDiamond = new Diamond()
            {
                ParticipantTimestamp = new DateTime(),
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };

            polishedDiamond = await _client.PolishDiamondAsync("ctestpolishsuccess", polishedDiamond);
                        
            Assert.Equal(LifecycleStateEnum.Polished, polishedDiamond.LifecycleState);
            Assert.Equal("ctestpolishsuccess", polishedDiamond.DiamondId);
            Assert.Equal("cl1234156789asdfgpolished", polishedDiamond.VersionId);
            Assert.Equal(VerificationStatusEnum.Verified, polishedDiamond.VerificationStatus);
        }
        
        [Fact]
        public async Task PolishUnknownRoughDiamondFailAsync()
        {
            var polishedDiamond = new Diamond()
            {
                ParticipantTimestamp = new DateTime(),
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };

            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () => 
                await _client.PolishDiamondAsync("ctestpolishunknownrough", polishedDiamond));
            
            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Diamond ctestpolishunknownrough could not be found.""
                    }", ex.Message);
        }

        [Fact]
        public async Task PolishIncompletePayloadDiamondFailAsync()
        {
            // polish a rough stone with an emtpy payload
            var polishedDiamond = new Diamond()
            {
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.PolishDiamondAsync("ctestincompletepayload", polishedDiamond));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }", ex.Message);
        }

        [Fact]
        public async Task PolishDiamondTooManyCaratsFailAsync()
        {
            // polish a rough stone with an emtpy payload
            var polishedDiamond = new Diamond()
            {
                ParticipantTimestamp = new DateTime(),
                Polished = new Polished()
                {
                    Carats = 10.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.PolishDiamondAsync("ctesttoomanycarats", polishedDiamond));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Polished diamond cannot be heavier than its rough counterpart""
                    }", ex.Message);
        }
    }

    public class UpdatePolishedDiamondTests : TestBase
    {
        public UpdatePolishedDiamondTests(ITestOutputHelper output) : base(output, InitMockService())
        { }


        private static HttpMessageHandler InitMockService()
        {
            //Setup Mock HttpMessageHandler
            var mockHttp = new MockHttpMessageHandler();

            var polishDiamondAnswer = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId="testid",
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfgupdate",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Polished,
                CurrentStage = "polished",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdatepolishedsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                },
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };

            // Setup a successfull response for polishing a rough diamond
            mockHttp.When(HttpMethod.Patch, "http://localhost/diamonds/ctestupdatepolishedsuccess/polished")
                .Respond("application/json", JsonConvert.SerializeObject(polishDiamondAnswer)); // Respond with JSONs

            // Setup a Not Found response when rough diamond id doesn't exists
            mockHttp.When(HttpMethod.Patch, "http://localhost/diamonds/ctestupdatepolishedunknownid/polished")
                .Respond(HttpStatusCode.NotFound,
                    "application/json",
                    @"{
                        ""message"": ""Diamond ctestupdatepolishedunknownid could not be found.""
                    }"
            );

            // Setup a Unprocessable Entity error when passing an incomplete payload
            mockHttp.When(HttpMethod.Patch, "http://localhost/diamonds/ctestupdatepolishedincompletepayload/polished")
                .Respond(
                    HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }");

            // Setup a Unprocessable Entity error when polished carats > rough carats
            mockHttp.When(HttpMethod.Patch, "http://localhost/diamonds/ctestupdatepolishedtoomanycarats/polished")
                .Respond(
                    HttpStatusCode.UnprocessableEntity,
                    "application/json",
                    @"{
                        ""message"": ""Polished diamond cannot be heavier than its rough counterpart""
                    }");

            return mockHttp;
        }


        [Fact]
        public async Task PolishedDiamondUpdateSuccessAsync()
        {
            var polishedDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId="testid",
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Polished,
                CurrentStage = "polished",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdatepolishedsuccess",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                },
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };

            polishedDiamond = await _client.UpdatePolishedDiamondAsync(polishedDiamond);

            Assert.Equal(LifecycleStateEnum.Polished, polishedDiamond.LifecycleState);
            Assert.Equal("ctestupdatepolishedsuccess", polishedDiamond.DiamondId);
            Assert.Equal("cl1234156789asdfgupdate", polishedDiamond.VersionId);
        }

        [Fact]
        public async Task UnknownPolishedDiamondUpdateFailAsync()
        {
            var polishedDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId="testid",
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Polished,
                CurrentStage = "polished",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdatepolishedunknownid",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                },
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };

            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.UpdatePolishedDiamondAsync(polishedDiamond));

            Assert.Equal(HttpStatusCode.NotFound, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Diamond ctestupdatepolishedunknownid could not be found.""
                    }", ex.Message);
        }

        [Fact]
        public async Task UpdatePolishedDiamondIncompletePayloadFailAsync()
        {
            // polish a rough stone with an emtpy payload
            var polishedDiamond = new Diamond()
            {
                ParticipantId="testid",
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Polished,
                CurrentStage = "polished",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdatepolishedincompletepayload",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                },
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                Polished = new Polished()
                {
                    Carats = 1.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.UpdatePolishedDiamondAsync(polishedDiamond));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": [
                            {
                                ""loc"": [
                                    ""body"",
                                    ""participant_timestamp""
                                ],
                                ""msg"": ""field required"",
                                ""type"": ""value_error.missing""
                            }
                        ]
                    }", ex.Message);
        }

        [Fact]
        public async Task PolishDiamondTooManyCaratsFailAsync()
        {
            // polish a rough stone with an emtpy payload
            var polishedDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.Now,
                ParticipantId="testid",
                SchemaVersion = "0.1.0",
                Id = new Guid(),
                IsCurrent = true,
                VersionId = "cl1234156789asdfg",
                SystemTimestamp = new DateTime(),
                LifecycleState = LifecycleStateEnum.Polished,
                CurrentStage = "polished",
                PossessionState = PossessionStateEnum.Held,
                DiamondId = "ctestupdatepolishedtoomanycarats",
                VerificationStatus = VerificationStatusEnum.Verified,
                VerificationDescription = "verified record",
                BoxId = "9999",
                SightNo=1,
                SightYear=2022,
                Provenance = new Provenance()
                {
                    ProvenanceName = "DTC",
                    ProvenanceType = ProvenanceTypeEnum.Entity
                },
                Rough = new Rough
                {
                    Carats = 7.500,
                },
                Polished = new Polished()
                {
                    Carats = 10.704,
                    CutGrade = CutGradeEnum.EX,
                    Shape = ShapeEnum.Princess,
                    Colour = ColourEnum.D,
                    Clarity = ClarityEnum.VS1,
                    Symmetry = SymmetryEnum.EX,
                    PolishQuality = PolishQualityEnum.EX,
                    FluorescenceIntensity = FluorescenceIntensityEnum.M,
                    FluorescenceColour = FluorescenceColourEnum.N
                }
            };
            TracrRequestException ex = await Assert.ThrowsAsync<TracrRequestException>(async () =>
                await _client.UpdatePolishedDiamondAsync(polishedDiamond));

            Assert.Equal(HttpStatusCode.UnprocessableEntity, ex.StatusCode);
            Assert.Equal(@"{
                        ""message"": ""Polished diamond cannot be heavier than its rough counterpart""
                    }", ex.Message);
        }
    }
}
