﻿# Tracr .NET Client

.NET implementation of a client for Tracr API.
For more details, please visit https://www.tracr.com

## How to instanciate a Tracr Client

Tracr Client is designed to be instanciated via dependency injection.
The client requires an option object to inject the OAuth 2.0 settings.

```
public class TracrClientOptions
{
    public string ApiUrl { get; set; } = "";
    public string TokenUrl { get; set; } = "";
    public string ClientId { get; set; } = "";
    public string ClientSecret { get; set; } = "";
}
```

```
ServiceCollection.AddTracrClient(
    TracrClientOptions clientOptions
);
```

## API Documentation

https://docs.tracr.com/docs/tracr-api

## Support

For any questions, please contact the technical team in https://support.tracr.com