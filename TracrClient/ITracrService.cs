using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using TracrClient.Models;

namespace TracrClient
{
    public interface ITracrService
    {
        // Rough Operations
        public Task<Diamond> CreateRoughDiamondAsync(Diamond diamond);
        public Task<Diamond> CreateRoughChildDiamondAsync(string parentDiamondId, Diamond diamond);
        public Task<Diamond> UpdateRoughDiamondAsync(Diamond diamond);
        
        

        // Split Operation
        public Task<Diamond> SplitDiamondAsync(string roughDiamondId, DateTime ParticipantTimestamp);
        
        // Polished Operations
        public Task<Diamond> PolishDiamondAsync(string roughDiamondId, Diamond diamond);
        public Task<Diamond> UpdatePolishedDiamondAsync(Diamond diamond);

        // Unlock Operation
        public Task<UnlockDiamondResponse> UnlockDiamondAsync(List<UnlockDiamondRequestItem> diamondsToUnlock);
        
        // Delete Operation
        public Task<DeleteDiamondResponse> DeleteDiamondAsync(List<DeleteDiamondRequestItem> diamondsToDelete);
        
        // Inventory
        public Task<InventoryQueryResults> GetDiamondsInventoryAsync(InventoryQuery query, int pageNumber, int pageSize);
        public Task<Diamond> GetDiamondAsync(string diamondId);

        // File Management
        public Task AppendScanFileAsync(string diamondId,
            string filePath,
            string description,
            string scanDeviceManufacturer,
            string scanDeviceModel,
            string scanSoftwareName,
            string scanSoftwareVersion);
        
        public Task AppendScanReportFileAsync(string diamondId,
            string filePath,
            string description,
            string scanDeviceManufacturer,
            string scanDeviceModel,
            string scanSoftwareName,
            string scanSoftwareVersion);

        public Task AppendPlanFileAsync(string diamondId,
            string filePath,
            string description,
            string scanDeviceManufacturer,
            string scanDeviceModel,
            string planSoftwareManufacturer,
            string planSoftwareName,
            string planSoftwareVersion);
        
        public Task AppendPlanReportAsync(string diamondId,
            string filePath,
            string description,
            string scanDeviceManufacturer,
            string scanDeviceModel,
            string planSoftwareManufacturer,
            string planSoftwareName,
            string planSoftwareVersion);

        public Task AppendImageFileAsync(string diamondId,
            string filePath,
            string description,
            ImageTypeEnum imageType);

        public Task AppendVideoFileAsync(string diamondId,
            string filePath,
            string description);

        public Task AppendSupplementaryFileAsync(string diamondId,
            string filePath,
            string description);
        
        public Task AppendGradingCertificateAsync(string diamondId,
            string filePath,
            string gradingLab,
            DateTime gradedDate,
            string graderCertificateNo,
            string graderReportNo,
            string graderInscriptionNo);
        
        public Task DownloadFileAsync(string diamondId,
            string hash,
            string filePath);

        // Shipment
        public Task<InboundShipment> GetInboundShipmentAsync(string shipmentId);

        public Task<List<InboundShipment>> GetAllInboundShipmentsAsync();

        public Task<OutboundShipment> GetOutboundShipmentAsync(string shipmentId);

        public Task<List<OutboundShipment>> GetAllOutboundShipmentsAsync();

        //Transfer
        public Task<List<TransferInAcceptResponseItem>> TransferInAcceptAsync(string shipmentId, List<TransferInAcceptItem> diamonds);

        public Task<List<TransferInRejectResponseItem>> TransferInRejectAsync(string shipmentId, List<TransferInRejectItem> diamondIds);

        public Task<TransferOutStartResponse> TransferOutStartAsync(string shipmentName, string receiver, List<string> diamondIds);

        public Task<List<TransferOutCancelResponseItem>> TransferOutCancelAsync(string shipmentId, List<TransferOutCancelItem> diamondIds);

    }
}