using System.Runtime.Serialization;

namespace TracrClient.Models
{ 
    public enum ProvenanceTypeEnum
    {
        Country,
        Entity
    }
    
    public enum ImageTypeEnum
    {
        Default,
        [EnumMember(Value = "3d")] ThreeD,
        B2B,
        Heart,
        Arrow,
        [EnumMember(Value = "aset_scope")] AsetScope,
        Plot, 
        [EnumMember(Value = "face_up")] FaceUp, 
        [EnumMember(Value = "dark_field")] DarkField, 
        [EnumMember(Value = "ideal_scope")] IdealScope, 
        Fluorescence, 
        [EnumMember(Value = "inclusion_map")] InclusionMap,
        Diagram
    }
    
    public enum PossessionStateEnum
    {
        Destroyed,
        Held,
        Missing,
        Staged,
        [EnumMember(Value = "in_transit")] InTransit,
        [EnumMember(Value = "in_grading")] InGrading,
        Known
    }

    public enum RomCaptureTypeEnum
    {
        Rom,
        Sightbox,
        Buyback
    }
    public enum LifecycleStateEnum
    {
        Rough,
        Polished,
        Split,
        Destroyed
    }
    
    public enum StageEnum
    {
        None,
        [EnumMember(Value = "rough")] Rough,
        [EnumMember(Value = "planning")] Planning,
        [EnumMember(Value = "sawing")] Sawing,
        [EnumMember(Value = "bruting")] Bruting,
        [EnumMember(Value = "polishing")] Polishing,
        [EnumMember(Value = "polished")] Polished
    }
    
    public enum VerificationStatusEnum
    {
        Pending,
        Unverifiable,
        Verified,
        Duplicate,
        Invalid,
        Error
    }

    public enum FluorescenceIntensityEnum
    {
        VS,
        S,
        M,
        F,
        SL,
        VSL,
        N
    }

    public enum CutGradeEnum
    {
        I,
        EX,
        VG,
        G,
        F,
        P,
        NA
    }

    public enum FluorescenceColourEnum
    {
        B,
        W,
        Y,
        O,
        R,
        G,
        N
    }

    public enum FancyColourEnum
    {
        BK,
        B,
        BN,
        CH,
        CM,
        CG,
        GY,
        G,
        O,
        P,
        PL,
        R,
        Y,
        V,
        W,
        X
    }

    public enum FancyColourIntensityEnum
    {
        F,
        VL,
        L,
        FL,
        FC,
        FCD,
        I,
        FV,
        D
    }

    public enum ShapeEnum
    {
        Asscher,
        Baguette,
        Briolette,
        Bullets,
        Calf,
        Cushion,
        Emerald,
        [EnumMember(Value = "European Cut")] EuropeanCut,
        Flanders,
        [EnumMember(Value = "Half Moon")] HalfMoon,
        Heart,
        Hexagonal,
        Kite,
        Lozenge,
        Marquise,
        Octagonal,
        [EnumMember(Value = "Old Miner")] OldMiner,
        Oval,
        Pear,
        Pentagonal,
        Princess,
        Radiant,
        Rose,
        Round,
        Shield,
        Square,
        [EnumMember(Value = "Square Emerald")] SquareEmerald,
        [EnumMember(Value = "Square Radiant")] SquareRadiant,
        Star,
        [EnumMember(Value = "Tapered Baguette")] TaperedBaguette,
        [EnumMember(Value = "Tapered Bullet")] TaperedBullet,
        Trapezoid,
        Triangle,
        Trilliant,
        [EnumMember(Value = "Cushion Modified Brilliant")] CushionModifiedBrilliant,
        Other
    }

    public enum ColourEnum
    {
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z
    }

    public enum ClarityEnum
    {
        FL,
        IF,
        LC,
        VVS1,
        VVS2,
        VS1,
        VS2,
        SI1,
        SI2,
        SI3,
        I1,
        I2,
        I3,
        P1,
        P2,
        P3
    }
    
    public enum SymmetryEnum
    {
        I,
        EX,
        [EnumMember(Value = "VG-EX")] VGEX,
        VG,
        [EnumMember(Value = "G-VG")] GVG,
        G,
        [EnumMember(Value = "F-G")] FG,
        F,
        P
    }
    
    public enum PolishQualityEnum
    {
        I,
        EX,
        [EnumMember(Value = "VG-EX")] VGEX,
        VG,
        [EnumMember(Value = "G-VG")] GVG,
        G,
        [EnumMember(Value = "F-G")] FG,
        F,
        P
    }

    public enum GirdleConditionEnum
    {
        B,
        F,
        P
    }
    public enum GirdleThicknessEnum
    {
        [EnumMember(Value = "Extremely Thick")] ExtremelyThick,
        [EnumMember(Value = "Very Thick")] VeryThick,
        [EnumMember(Value = "Thick")] Thick,
        [EnumMember(Value = "Slightly Thick")] SlightlyThick,
        [EnumMember(Value = "Medium")] Medium,
        [EnumMember(Value = "Thin")] Thin,
        [EnumMember(Value = "Slightly Thin")] SlightlyThin,
        [EnumMember(Value = "Very Thin")] VeryThin,
        [EnumMember(Value = "Extremely Thin")] ExtremelyThin
    }
    
    public enum CuletSizeEnum
    {
        EL,
        VL,
        L,
        SL,
        M,
        S,
        VS,
        N
    }

    public enum CuletConditionEnum
    {
        P,
        A,
        C
    }

    public enum TransferEnum
    {
        Accepted,
        Cancelled,
        Owned,
        [EnumMember(Value = "pending_reject")] PendingReject,
        [EnumMember(Value = "pending_verification")] PendingVerification,
        Rejected,
        Transferred,
        [EnumMember(Value = "transfer_in_confirmed")] TransferInConfirmed,
        [EnumMember(Value = "transfer_in_pending")] TransferInPending,
        [EnumMember(Value = "verification_failed")] VerificationFailed,
        [EnumMember(Value = "pending_cancel")] PendingCancel,
        [EnumMember(Value = "transfer_out_confirmed")] TransferOutConfirmed,
        [EnumMember(Value = "transfer_out_pending")] TransferOutPending,
        Verified
    }

    public enum TransferStatusEnum
    {
        Success,
        [EnumMember(Value = "diamond_not_found")] DiamondNotFound,
        [EnumMember(Value = "inscription_number_invalid")] InscriptionNumberInvalid,
        [EnumMember(Value = "diamond_in_incorrect_state")] DiamondInIncorrectState,
        [EnumMember(Value = "diamond_transfer_cancelled")] DiamondTransferCancelled,
        [EnumMember(Value = "diamond_transfer_complete")] DiamondTransferComplete,
        [EnumMember(Value = "diamond_transfer_rejected")] DiamondTransferRejected,
        [EnumMember(Value = "diamond_already_pending_transfer")] DiamondAlreadyPendingTransfer,
        [EnumMember(Value = "diamond_already_transferred")] DiamondAlreadyTransferred,
        [EnumMember(Value = "diamond_does_not_exist")] DiamondDoesNotExist,
        [EnumMember(Value = "diamond_has_no_inscription_numbers")] DiamondHasNoInscriptionNumbers,
        [EnumMember(Value = "diamond_in_terminal_state")] DiamondInTerminalState,
        [EnumMember(Value = "diamond_lifecycle_state_not_transferrable")] DiamondLifecycleStateNotTransferrable
    }

}