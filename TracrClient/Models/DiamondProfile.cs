using AutoMapper;

namespace TracrClient.Models
{
    public class DiamondProfile : Profile
    {
        public DiamondProfile()
        {
            CreateMap<Diamond, RoughOperation>();
            CreateMap<Diamond, PolishedOperation>();
        }
    }
}