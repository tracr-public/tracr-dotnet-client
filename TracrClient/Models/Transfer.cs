﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using TracrClient.Models;

namespace TracrClient.Models
{
    public class TransferInAcceptItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("inscription_number")] public string? InscriptionNumber { get; set; }
    }

    public class TransferInAcceptResponseItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("inscription_number")] public string? InscriptionNumber { get; set; }
        [JsonProperty("status")] public TransferStatusEnum? Status { get; set; }
    }

    public class TransferInRejectItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
    }

    public class TransferInRejectResponseItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("status")] public TransferStatusEnum? Status { get; set; }
    }

    public class TransferOutCancelItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
    }

    public class TransferOutCancelResponseItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("status")] public TransferStatusEnum? Status { get; set; }
    }


    public class TransferOutStartOperation
    {
        [JsonProperty("shipment_name")] public string? ShipmentName { get; set; }

        [JsonProperty("diamonds")] public List<TransferOutStartItem>? Diamonds { get; set; }

        [JsonProperty("receiver")] public string? Receiver { get; set; }
    }

    public class TransferOutStartItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
    }

    public class TransferOutStartResponse
    {
        [JsonProperty("shipment_id")] public string? ShipmentId { get; set; }
        [JsonProperty("shipment_name")] public string? ShipmentName { get; set; }

        [JsonProperty("diamonds")] public List<TransferOutStartResponseItem>? Diamonds { get; set; }
        
    }

    public class TransferOutStartResponseItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("status")] public TransferStatusEnum? Status { get; set; }

    }
}
