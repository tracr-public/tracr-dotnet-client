﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TracrClient.Models
{
    public enum UnlockStatusEnum
    {
        None,
        [EnumMember(Value = "success")] Success,
        [EnumMember(Value = "diamond_does_not_exist")] DiamondDoesNotExist,
        [EnumMember(Value = "diamond_cannot_be_unlocked")] DiamondCannotBeUnlocked
    }

    public class UnlockDiamondRequest
    {
        [JsonProperty("diamonds")] public List<UnlockDiamondRequestItem>? Diamonds { get; set; }
    }

    public class UnlockDiamondRequestItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
    }

    public class UnlockDiamondResponseItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("status")] public UnlockStatusEnum? Status { get; set; }
    }

    public class UnlockDiamondResponse
    {
        [JsonProperty("diamonds")] public List<UnlockDiamondResponseItem>? Diamonds { get; set; }
    }

    
}
