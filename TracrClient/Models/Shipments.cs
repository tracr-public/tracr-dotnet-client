﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using TracrClient.Models;

namespace TracrClient.Models
{
    public class InboundShipment
    {
            [JsonProperty("id")] public string? Id { get; set; }

            [JsonProperty("name")] public string? Name { get; set; }

            [JsonProperty("sender")] public string? Sender { get; set; }

            [JsonProperty("created_at")] public DateTime? CreatedAt { get; set; }

            public List<ShipmentDiamond>? Diamonds { get; set; }
    }

    public class AllInboundShipments
    {
        public List<InboundShipment>? Items { get; set; }
    }


    public class OutboundShipment
    {
        [JsonProperty("id")] public string? Id { get; set; }

        [JsonProperty("name")] public string? Name { get; set; }

        [JsonProperty("receiver")] public string? Receiver { get; set; }

        [JsonProperty("created_at")] public DateTime? CreatedAt { get; set; }

        public List<ShipmentDiamond>? Diamonds { get; set; }
    }

    public class AllOutboundShipments
    {
        public List<OutboundShipment>? Items { get; set; }
    }

    public class ShipmentDiamond
    {
        public double? Carats { get; set; }
        public ClarityEnum Clarity { get; set; }
        public ColourEnum Colour { get; set; }
        [JsonProperty("created_at")] public DateTime? CreatedAt { get; set; }
        [JsonProperty("cut_grade")] public CutGradeEnum? CutGrade { get; set; }
        public ShapeEnum Shape { get; set; }
        [JsonProperty("transfer_state")] public TransferEnum? TransferState { get; set; }
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("updated_at")] public DateTime? UpdatedAt { get; set; }
        [JsonProperty("inscription_numbers")] public List<string>? InscriptionNumbers { get; set; }
        
    }
}
