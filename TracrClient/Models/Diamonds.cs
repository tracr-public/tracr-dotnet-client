using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace TracrClient.Models
{
    public class Diamond
    {
        [JsonProperty("schema_version")] public string? SchemaVersion { get; set; }
        [JsonProperty("id")] public Guid Id { get; set; }
        [JsonProperty("parent_id")] public Guid? ParentId { get; set; }
        [JsonProperty("is_current")] public bool? IsCurrent { get; set; }
        [JsonProperty("version_id")] public string? VersionId { get; set; }
        [JsonProperty("system_timestamp")] public DateTime? SystemTimestamp { get; set; }
        [JsonProperty("lifecycle_state")] public LifecycleStateEnum? LifecycleState { get; set; }
        [JsonProperty("current_stage")] public string? CurrentStage { get; set; }
        [JsonProperty("possession_state")] public PossessionStateEnum? PossessionState { get; set; }
        [JsonProperty("transfer_destination_platform")] public string? TransferDestinationPlatform { get; set; }
        [JsonProperty("participant_id")] public string? ParticipantId { get; set; }
        [JsonProperty("participant_parent_id")] public string? ParticipantParentId { get; set; }
        [JsonProperty("participant_timestamp")] public DateTime? ParticipantTimestamp { get; set; }
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("diamond_parent_id")] public string? DiamondParentId { get; set; }
        [JsonProperty("verification_status")] public VerificationStatusEnum? VerificationStatus { get; set; }
        [JsonProperty("verification_status_modified_at")] public DateTime? VerificationStatusModifiedAt { get; set; }
        [JsonProperty("verification_description")] public string? VerificationDescription { get; set; }
        [JsonProperty("verification_code")] public string? VerificationCode { get; set; }
        [JsonProperty("box_id")] public string? BoxId { get; set; }
        [JsonProperty("box_description")] public string? BoxDescription { get; set; }
        [JsonProperty("sight_no")] public int? SightNo { get; set; }
        [JsonProperty("sight_year")] public int? SightYear { get; set; }
        [JsonProperty("entity_specific_tags")] public Dictionary<String,object>? EntitySpecificTags { get; set; }
        [JsonProperty("supplementary_tags")] public Dictionary<String,object>? SupplementaryTags { get; set; }

        public Provenance? Provenance { get; set; }
        
        public Rough? Rough { get; set; }
 
        public Polished? Polished { get; set; }
    }

    public class Provenance
    {
        [JsonProperty("provenance_name")] public string? ProvenanceName { get; set; }

        [JsonProperty("provenance_type")] public ProvenanceTypeEnum? ProvenanceType { get; set; }
        
        [JsonProperty("mine_name")] public string? MineName { get; set; }
    }

    public class Rough
    {
        public double Carats { get; set; }
        public List<ImageRef>? Images { get; set; }
        public string? Model { get; set; }
        public ColourEnum? Colour { get; set; }
        public string? Quality { get; set; }
        public string? Stress { get; set; }
        [JsonProperty("fluorescence_intensity")] public FluorescenceIntensityEnum? FluorescenceIntensity { get; set; }
        [JsonProperty("fluorescence_colour")] public FluorescenceColourEnum? FluorescenceColour { get; set; }
        [JsonProperty("fancy_colour")] public FancyColourEnum? FancyColour { get; set; }
        [JsonProperty("fancy_colour_intensity")] public FancyColourIntensityEnum? FancyColourIntensity { get; set; }
        [JsonProperty("fancy_colour_overtone")] public string? FancyColourOvertone { get; set; }
        public ScanRef? Scan { get; set; }
        [JsonProperty("scan_reports")] public List<ScanRef>? ScanReports { get; set; }
        public PlanRef? Plan { get; set; }
        [JsonProperty("plan_reports")] public List<PlanRef>? PlanReports { get; set; }
        public List<FileRef>? Videos { get; set; }
        [JsonProperty("supplementary_files")] public List<FileRef>? SupplementaryFiles { get; set; }
        [JsonProperty("silhouettes")] public List<SilhouetteRef>? Silhouettes { get; set; }

        public bool ShouldSerializeScan(){ return false; }
        public bool ShouldSerializeScanReports(){ return false; }
        public bool ShouldSerializePlan(){ return false; }
        public bool ShouldSerializePlanReports(){ return false; }
        public bool ShouldSerializeSupplementaryFiles(){ return false; }
        public bool ShouldSerializeSilhouettes() { return false; }
        public bool ShouldSerializeVideos(){ return false; }
        public bool ShouldSerializeImages(){ return false; }
    }
    
    public class Polished
    {
        public double? Carats { get; set; }
        public List<ImageRef>? Images { get; set; }
        [JsonProperty("cut_grade")] public CutGradeEnum? CutGrade { get; set; }
        public ShapeEnum? Shape { get; set; }
        public ColourEnum? Colour { get; set; }
        public ClarityEnum? Clarity { get; set; }
        public SymmetryEnum? Symmetry { get; set; }
        [JsonProperty("polish_quality")] public PolishQualityEnum? PolishQuality { get; set; }
        [JsonProperty("fluorescence_intensity")] public FluorescenceIntensityEnum? FluorescenceIntensity { get; set; }
        [JsonProperty("fluorescence_colour")] public FluorescenceColourEnum? FluorescenceColour { get; set; }
        public ScanRef? Scan { get; set; }
        [JsonProperty("scan_reports")] public List<ScanRef>? ScanReports { get; set; }
        [JsonProperty("supplementary_files")] public List<FileRef>? SupplementaryFiles { get; set; }
        public List<FileRef>? Videos { get; set; }
        [JsonProperty("fancy_colour")] public FancyColourEnum? FancyColour { get; set; }
        [JsonProperty("fancy_colour_intensity")] public FancyColourIntensityEnum? FancyColourIntensity { get; set; }
        [JsonProperty("fancy_colour_overtone")] public string? FancyColourOvertone { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Depth { get; set; }
        [JsonProperty("depth_percent")] public double? DepthPercent { get; set; }
        [JsonProperty("table_percent")] public double? TablePercent { get; set; }
        [JsonProperty("girdle_thickness_from")] public GirdleThicknessEnum? GirdleThicknessFrom { get; set; }
        [JsonProperty("girdle_thickness_to")] public GirdleThicknessEnum? GirdleThicknessTo { get; set; }
        [JsonProperty("girdle_condition")] public GirdleConditionEnum? GirdleCondition { get; set; }
        [JsonProperty("pavillion_angle")] public double? PavillionAngle { get; set; }
        [JsonProperty("pavillion_percent")] public double? PavillionPercent { get; set; }
        [JsonProperty("culet_size")] public string? CuletSize { get; set; }
        [JsonProperty("culet_condition")] public string? CuletCondition { get; set; }
        [JsonProperty("grading_certificates")] public List<GradingCertificateRef>? GradingCertificates { get; set; }
        [JsonProperty("silhouettes")] public List<SilhouetteRef>? Silhouettes { get; set; }
        [JsonProperty("crown_height")] public double? CrownHeight { get; set; }
        [JsonProperty("crown_angle")] public double? CrownAngle { get; set; }
        [JsonProperty("diameter_minimum")] public double? DiameterMinimum { get; set; }
        [JsonProperty("diameter_maximum")] public double? DiameterMaximum { get; set; }
        [JsonProperty("entity_specific_tags")] public Dictionary<String, object>? EntitySpecificTags { get; set; }
        [JsonProperty("supplementary_tags")] public Dictionary<String, object>? SupplementaryTags { get; set; }

        public bool ShouldSerializeScan() { return false; }
        public bool ShouldSerializeScanReports() { return false; }
        public bool ShouldSerializeSupplementaryFiles() { return false; }
        public bool ShouldSerializeVideos() { return false; }
        public bool ShouldSerializeImages() { return false; }
        public bool ShouldSerializeGradingCertificates() { return false; }
    }
    
    public class ScanRef
    {
        [JsonProperty("filename")] public string? Filename { get; set; }
        [JsonProperty("description")] public string? Description { get; set; }
        [JsonProperty("hash")] public string? Hash { get; set; }
        [JsonProperty("scan_device_manufacturer")] public string? ScanDeviceManufacturer { get; set; }
        [JsonProperty("scan_device_model")] public string? ScanDeviceModel { get; set; }
        [JsonProperty("scan_software_name")] public string? ScanSoftwareName { get; set; }
        [JsonProperty("scan_software_version")] public string? ScanSoftwareVersion { get; set; }
    }
    
    public class PlanRef
    {
        [JsonProperty("filename")] public string? Filename { get; set; }
        [JsonProperty("description")] public string? Description { get; set; }
        [JsonProperty("hash")] public string? Hash { get; set; }
        [JsonProperty("scan_device_manufacturer")] public string? ScanDeviceManufacturer { get; set; }
        [JsonProperty("scan_device_model")] public string? ScanDeviceModel { get; set; }

        [JsonProperty("plan_software_manufacturer")] public string? PlanSoftwareManufacturer { get; set; }
        [JsonProperty("plan_software_name")] public string? PlanSoftwareName { get; set; }
        [JsonProperty("plan_software_version")] public string? PlanSoftwareVersion { get; set; }
    }
    
    public class ImageRef
    {
        public string? FileName { get; set; }
        public string Description { get; set; }
        public string Hash { get; set; }
        [JsonProperty("image_type")] public ImageTypeEnum? ImageType { get; set; }
    }

    public class FileRef
    {
        [JsonProperty("filename")] public string? FileName { get; set; }
        [JsonProperty("description")] public string? Description { get; set; }
        [JsonProperty("hash")] public string? Hash { get; set; }
    }

    public class SilhouetteRef
    {
        [JsonProperty("filename")] public string? FileName { get; set; }
        [JsonProperty("description")] public string? Description { get; set; }
        [JsonProperty("hash")] public string? Hash { get; set; }
        [JsonProperty("scan_hash")] public string? ScanHash { get; set; }
        [JsonProperty("image_height")] public string? ImageHeight { get; set; }
        [JsonProperty("image_weight")] public string? ImageWeight { get; set; }

    }
    public class GradingCertificateRef
    {
        [JsonProperty("filename")] public string? Filename { get; set; }
        [JsonProperty("description")] public string? Description { get; set; }
        [JsonProperty("hash")] public string? Hash { get; set; }
        [JsonProperty("grading_lab")] public string? GradingLab { get; set; }
        [JsonProperty("graded_date")] public string? GradedDate { get; set; }
        [JsonProperty("grader_certificate_no")] public string? GraderCertificateNo { get; set; }
        [JsonProperty("grader_report_no")] public string? GraderReportNo { get; set; }
        [JsonProperty("grader_inscription_no")] public string? GraderInscriptionNo { get; set; }
    }
    
    public class RoughOperation
    {
        [JsonProperty("box_id")] public string? BoxId { get; set; }
        [JsonProperty("sight_no")] public int? SightNo { get; set; }
        [JsonProperty("sight_year")] public int? SightYear { get; set; }
        [JsonProperty("participant_id")] public string? ParticipantId { get; set; }
        [JsonProperty("participant_timestamp")] public DateTime? ParticipantTimestamp { get; set; }
        [JsonProperty("entity_specific_tags")] public Dictionary<String,object>? EntitySpecificTags { get; set; }
        [JsonProperty("supplementary_tags")] public Dictionary<String,object>? SupplementaryTags { get; set; }
        [JsonProperty("provenance")] public Provenance? Provenance { get; set; }
        [JsonProperty("rough")] public Rough? Rough { get; set; }
    }
    
    public class PolishedOperation
    {
        [JsonProperty("participant_id")] public string? ParticipantId { get; set; }
        [JsonProperty("participant_timestamp")] public DateTime? ParticipantTimestamp { get; set; }
        [JsonProperty("entity_specific_tags")] public Dictionary<String, object>? EntitySpecificTags { get; set; }
        [JsonProperty("supplementary_tags")] public Dictionary<String, object>? SupplementaryTags { get; set; }
        [JsonProperty("polished")] public Polished? Polished { get; set; }
    }
    
    public class SplitOperation
    {
        [JsonProperty("participant_timestamp")] public DateTime? ParticipantTimestamp { get; set; }
        [JsonProperty("entity_specific_tags")] public Dictionary<String, object>? EntitySpecificTags { get; set; }
    }
}
