﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TracrClient.Models
{
    public enum DeleteStatusEnum
    {
        None,
        [EnumMember(Value = "success")] Success,
        [EnumMember(Value = "descendants_in_active_shipment")] DescendantsInActiveShipment,
        [EnumMember(Value = "descendants_transferred")] DescendantsTransferred,
        [EnumMember(Value = "diamond_has_descendants")] DiamondHasDescendants,
        [EnumMember(Value = "diamond_in_active_shipment")] DiamondInActiveShipment,
        [EnumMember(Value = "diamond_not_found")] DiamondNotFound,
        [EnumMember(Value = "diamond_transferred")] DiamondTransferred
    }

    public class DeleteDiamondRequestItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("delete_descendants")] public bool? DeleteDescendants { get; set; }
    }

    public class DeleteDiamondRequest
    {
        [JsonProperty("diamonds")] public List<DeleteDiamondRequestItem>? Diamonds { get; set; }
    }

    public class DeleteDiamondResponseDescendant
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("participant_id")] public string? ParticipantId { get; set; }
    }

    public class DeleteDiamondResponseItem
    {
        [JsonProperty("diamond_id")] public string? DiamondId { get; set; }
        [JsonProperty("participant_id")] public string? ParticipantId { get; set; }
        [JsonProperty("status")] public DeleteStatusEnum? Status { get; set; }
        [JsonProperty("descendants")] public List<DeleteDiamondResponseDescendant>? Descendants { get; set; }

        [JsonProperty("error")] public string? Error { get; set; }
    }

    public class DeleteDiamondResponse
    {
        [JsonProperty("results")] public List<DeleteDiamondResponseItem>? Results { get; set; }
    }

    
}
