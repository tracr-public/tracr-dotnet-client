using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;


namespace TracrClient.Extensions
{
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            ContractResolver = new DefaultContractResolver()
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            },
            Converters = new List<JsonConverter>
            (
            new JsonConverter[]
                {
                    new StringEnumConverter()
                }
            )
        };
    }
    
    public class TracrRequestException : Exception
    {
        public HttpStatusCode StatusCode;

        public TracrRequestException()
        {
        }

        public TracrRequestException(string message, HttpStatusCode httpStatusCode) : base(message)
        {
            StatusCode = httpStatusCode;
        }

        public TracrRequestException(string message, HttpStatusCode httpStatusCode, Exception inner) : base(message, inner)
        {
            StatusCode = httpStatusCode;
        }

        protected TracrRequestException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
    
    public static class HttpClientExtensions {
        
        public static async Task<T> GetObjectAsync<T>(this HttpClient client, string url)
        {
            using var response = await client.GetAsync(url);
            
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new TracrRequestException(message, response.StatusCode, e);
            }

            using var stream = await response.Content.ReadAsStreamAsync();
            using var sr = new StreamReader(stream);
            using JsonReader reader = new JsonTextReader(sr);
            var serializer = JsonSerializer.CreateDefault(Converter.Settings);
            var deserializedData = serializer.Deserialize<T>(reader);
            return deserializedData;
        }

        public static async Task<T> GetArrayObjectAsync<T>(this HttpClient client, string url)
        {
            using var response = await client.GetAsync(url);

            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new TracrRequestException(message, response.StatusCode, e);
            }

            using var stream = await response.Content.ReadAsStreamAsync();
            using var sr = new StreamReader(stream);
            String jsonResponse = sr.ReadToEnd();
            var serializer = JsonSerializer.CreateDefault(Converter.Settings);
            using JsonReader reader = new JsonTextReader(sr);
            var deserializedData = serializer.Deserialize<T>(reader);
            return deserializedData;
        }

        public static async Task<TOut> PostObjectAsync<TIn, TOut>(this HttpClient client, string url, TIn obj)
        {
            return await client.PostObjectAsync<TIn, TOut>(url, obj, Converter.Settings);
        }

        public static async Task<TOut> PostObjectAsync<TIn, TOut>(this HttpClient client, string url, TIn obj, JsonSerializerSettings settings)
        {
            var objStr = JsonConvert.SerializeObject(obj, settings);

            using HttpContent content = new StringContent(objStr, Encoding.UTF8, "application/json");
            using var response = await client.PostAsync(url, content);
            
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new TracrRequestException(message, response.StatusCode, e);
            }

            using var stream = await response.Content.ReadAsStreamAsync();
            using var sr = new StreamReader(stream);
            using JsonReader reader = new JsonTextReader(sr);
            var serializer = JsonSerializer.CreateDefault(settings);
            var deserializedData = serializer.Deserialize<TOut>(reader);
            return deserializedData;
        }
        
        public static async Task PostFileAsync(this HttpClient client, string uri, MultipartFormDataContent content)
        {
            var request = new HttpRequestMessage()
            {
                Content = content,
                Method = HttpMethod.Post,
                RequestUri = new Uri(client.BaseAddress + uri)
            };

            var response = await client.SendAsync(request);
            
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new TracrRequestException(message, response.StatusCode, e);
            }
        }

        public static async Task<TOut> PatchObjectAsync<TIn, TOut>(this HttpClient client, string url, TIn obj)
        {
            var objStr = JsonConvert.SerializeObject(obj, Converter.Settings);

            using HttpContent content = new StringContent(objStr, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage()
            {
                Content = content,
                Method = new HttpMethod("PATCH"),
                RequestUri = new Uri(client.BaseAddress + url)
            };
                    
            var response = await client.SendAsync(request);
            
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new TracrRequestException(message, response.StatusCode, e);
            }

            var stream = await response.Content.ReadAsStreamAsync();
            using var sr = new StreamReader(stream);
            using JsonReader reader = new JsonTextReader(sr);
            var serializer = JsonSerializer.CreateDefault(Converter.Settings);
            var deserializedData = serializer.Deserialize<TOut>(reader);
            return deserializedData;
        }
        
        public static async Task<TOut> DeleteObjectAsync<TIn, TOut>(this HttpClient client, string url, TIn obj)
        {
            var objStr = JsonConvert.SerializeObject(obj, Converter.Settings);

            using HttpContent content = new StringContent(objStr, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage()
            {
                Content = content,
                Method = HttpMethod.Delete,
                RequestUri = new Uri(client.BaseAddress + url)
            };
                    
            var response = await client.SendAsync(request);
            
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new TracrRequestException(message, response.StatusCode, e);
            }
            
            var stream = await response.Content.ReadAsStreamAsync();
            using var sr = new StreamReader(stream);
            using JsonReader reader = new JsonTextReader(sr);
            var serializer = JsonSerializer.CreateDefault(Converter.Settings);
            var deserializedData = serializer.Deserialize<TOut>(reader);
            return deserializedData;
        }
    }
}