using System;
using System.Net.Http;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using TracrClient.Models;
using Microsoft.AspNetCore.DataProtection;
using TracrClient.Security;

namespace TracrClient.Extensions
{
    public class TracrClientOptions
    {
        public string ApiUrl { get; set; } = "";
        public string TokenUrl { get; set; } = "";
        public string ClientId { get; set; } = "";
        public string ClientSecret { get; set; } = "";
    }

    public static class TracrClientExtensions {

        public static IServiceCollection AddTracrClient(this IServiceCollection services, TracrClientOptions clientOptions)
        {
            services.AddDataProtection()
                .PersistKeysToFileSystem(new System.IO.DirectoryInfo("."))
                .SetApplicationName("Tracr.Client")
                .SetDefaultKeyLifetime(TimeSpan.FromDays(180));
            

            services.AddOptions<TokenManagementOptions>().Configure(options =>
            {
                options.Audience = clientOptions.ApiUrl;
                options.ClientId = clientOptions.ClientId;
                options.ClientSecret = clientOptions.ClientSecret;
                options.TokenUrl = clientOptions.TokenUrl;
            });

            services.AddSingleton<ITokenManagementService, TokenManagementService>();

            services.AddHttpClient<TracrService>(configureClient: c =>
                {
                    c.BaseAddress = new Uri(clientOptions.ApiUrl);
                    c.DefaultRequestHeaders.Add("Accept", "application/json");
                    c.DefaultRequestHeaders.Add("User-Agent", "Tracr-Api-Client");
                })
                .AddPolicyHandler(
                    HttpPolicyExtensions
                    .HandleTransientHttpError()
                    .WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2,
                        retryAttempt)))
            ).AddClientAccessTokenHandler();

            services.AddAutoMapper(typeof(DiamondProfile));

            services.AddScoped<ITracrService, TracrService>();
            
            return services;
        }

        public static IServiceCollection AddTracrClient(this IServiceCollection services, TracrClientOptions clientOptions, HttpMessageHandler messageHandler)
        {
            services.AddHttpClient<TracrService>(configureClient: c =>
            {
                c.BaseAddress = new Uri(clientOptions.ApiUrl);
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                c.DefaultRequestHeaders.Add("User-Agent", "Tracr-Api-Client");
            }).ConfigurePrimaryHttpMessageHandler(() => messageHandler);

            services.AddAutoMapper(typeof(DiamondProfile));

            services.AddScoped<ITracrService, TracrService>();

            return services;
        }
    }
}