using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;

using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TracrClient.Extensions;

namespace TracrClient.Security
{

    public class TokenManagementOptions
    {
        public string TokenUrl { get; set; } = "";
        public string ClientId { get; set; } = "";
        public string ClientSecret { get; set; } = "";
        public string Audience { get; set; } = "";
    }

    public class TokenRequest
    {
        public string ClientId { get; set; } = "";
        public string ClientSecret { get; set; } = "";
        public string Audience { get; set; } = "";
        public string GrantType { get; set; } = "client_credentials";
    }

    public class TokenResponse
    {
        public string? AccessToken { get; set; }
        public string? Scope { get; set; }
        public long? ExpiresIn { get; set; }
        public string? TokenType { get; set; }
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            ContractResolver = new DefaultContractResolver()
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            },
            Converters = new List<JsonConverter>
            (
            new JsonConverter[]
                {
                    new StringEnumConverter()
                }
            )
        };
    }

    internal class TokenManagementService : ITokenManagementService
    {
        private string? _token;
        private readonly IDataProtector _protector;
        private readonly HttpClient _client;
        private readonly IOptions<TokenManagementOptions> _options;
        private readonly ILogger<TokenManagementService> _logger;

        public TokenManagementService(
            IDataProtectionProvider provider, 
            HttpClient httpClient,
            ILogger<TokenManagementService> logger,
            IOptions<TokenManagementOptions> options)
        {
            _protector = provider.CreateProtector("Tracr.Client.JWT.Store");
            _client= httpClient;
            _logger= logger;
            _options=options;
            
        }

        public static long GetTokenExpirationTime(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            var tokenExp = jwtSecurityToken.Claims.First(claim => claim.Type.Equals("exp")).Value;
            var ticks = long.Parse(tokenExp);
            return ticks;
        }

        public static bool CheckTokenIsValid(string? token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                return false;
            }

            var tokenTicks = GetTokenExpirationTime(token);
            tokenTicks = tokenTicks - 60; // Hack to force to renew the token 1min before expiry (to avoid time drift between local systen and Cloudflare)

            var tokenDate = DateTimeOffset.FromUnixTimeSeconds(tokenTicks).UtcDateTime;

            var now = DateTime.Now.ToUniversalTime();

            var valid = tokenDate >= now;

            return valid;
        }

        public async Task<string> GetAccessTokenAsync(CancellationToken cancellationToken)
        {
            var tokenFile = "./token.dat";

            // Check validity of the current token
            if (CheckTokenIsValid(_token))
            {
                _logger.LogDebug($"In Memory token is valid");
                return _token;
            }

            _logger.LogDebug($"In Memory token is not valid");

            cancellationToken.ThrowIfCancellationRequested();

            // Get the token from the store if it exists
            if (File.Exists(tokenFile))
            {
                var encryptedToken = File.ReadAllText(tokenFile);

                if (encryptedToken != "")
                {
                    _logger.LogDebug($"Found token in persistance file");

                    // Decrypt the tokens
                    var token = _protector.Unprotect(encryptedToken);

                    // Check validity of the current token
                    if (CheckTokenIsValid(token))
                    {
                        _logger.LogDebug($"Token in persistance file is valid");

                        // update in memory token
                        _token = token;

                        return token;
                    }
                }
            }

            cancellationToken.ThrowIfCancellationRequested();

            _logger.LogDebug($"Requesting a fresh token");

            // Get a fresh token
            var tokenResponse = await _client.PostObjectAsync<TokenRequest, TokenResponse>(_options.Value.TokenUrl, new TokenRequest
                {
                    Audience= _options.Value.Audience,
                    ClientId= _options.Value.ClientId,
                    ClientSecret= _options.Value.ClientSecret
                }, Converter.Settings
            );

            // Update in memory token
            _token = tokenResponse.AccessToken;

            _logger.LogDebug($"Token : {_token}");

            cancellationToken.ThrowIfCancellationRequested();

            // Protect and store
            var securedToken = _protector.Protect(_token);
            File.WriteAllText(tokenFile, securedToken);

            _logger.LogDebug($"Token {securedToken} persisted in a local file");

            return tokenResponse.AccessToken;
        }
    }
}
