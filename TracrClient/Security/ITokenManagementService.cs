﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TracrClient.Security
{
    public interface ITokenManagementService
    {
        public Task<string> GetAccessTokenAsync(CancellationToken cancellationToken);
    }
}
