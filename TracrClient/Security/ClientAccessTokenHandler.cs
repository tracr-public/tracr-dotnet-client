﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TracrClient.Security
{
    public static class TracrClientSecurityExtensions
    {
        public static IHttpClientBuilder AddClientAccessTokenHandler(
            this IHttpClientBuilder httpClientBuilder)
        {
            return httpClientBuilder.AddHttpMessageHandler(provider =>
            {
                var accessTokenManagementService = provider.GetRequiredService<ITokenManagementService>();

                return new ClientAccessTokenHandler(accessTokenManagementService);
            });
        }
    }

    public class ClientAccessTokenHandler : DelegatingHandler
    {
        private readonly ITokenManagementService _tokenManagementService;

        public ClientAccessTokenHandler(ITokenManagementService tokenManagementService)
        {
            _tokenManagementService = tokenManagementService;
        }
        public async Task<string> GetAccessTokenAsync(CancellationToken cancellationToken)
        {
            return await _tokenManagementService.GetAccessTokenAsync(cancellationToken);
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            await SetTokenAsync(request, cancellationToken);
            var response = await base.SendAsync(request, cancellationToken);

            return response;
        }

        protected virtual async Task SetTokenAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var token = await GetAccessTokenAsync(cancellationToken);

            if (!string.IsNullOrWhiteSpace(token))
            {
                request.Headers.Authorization = new AuthenticationHeaderValue(token);
            }
        }
    }
}
