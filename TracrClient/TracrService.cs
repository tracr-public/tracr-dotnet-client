﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using AutoMapper;
using TracrClient.Models;
using TracrClient.Extensions;
using Newtonsoft.Json;

namespace TracrClient
{
    public class InventoryQuery
    {
        public List<string>? DiamondIds { get; set; }
        public List<string>? ParticipantIds { get; set; }
        public List<LifecycleStateEnum>? LifecycleStates { get; set; }
        public List<PossessionStateEnum>? PossessionStates { get; set; }
    }

    public class InventoryQueryResults
    {
        public int Page { get; set; }
        public int Pages { get; set; }
        public int Total { get; set; }
        public List<Diamond> Results { get; set; }
        public string? Previous { get; set; }
        public string? Next { get; set; }
    }

    public class TracrService : ITracrService, IDisposable
    {
        private readonly HttpClient _client;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public TracrService(ILoggerFactory loggerFactory, HttpClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _logger = loggerFactory.CreateLogger<TracrService>();
            _mapper = mapper;
        }
        
        // Rough Operations
        public async Task<Diamond> CreateRoughDiamondAsync(Diamond diamond)
        {
            var roughOperation = _mapper.Map<Diamond, RoughOperation>(diamond);

            var diamondResult = await _client.PostObjectAsync<RoughOperation,Diamond>(
                $"/diamonds/rough/create", roughOperation);
            return diamondResult;
        }

        public async Task<Diamond> CreateRoughChildDiamondAsync(string parentDiamondId, Diamond diamond)
        {
            var roughOperation = _mapper.Map<Diamond, RoughOperation>(diamond);

            var diamondResult = await _client.PostObjectAsync<RoughOperation,Diamond>(
                $"/diamonds/{parentDiamondId}/rough/create", roughOperation);
            return diamondResult;
        }

        public async Task<Diamond> UpdateRoughDiamondAsync(Diamond diamond)
        {
            var roughOperation = _mapper.Map<Diamond, RoughOperation>(diamond);
            
            var diamondResult = await _client.PatchObjectAsync<RoughOperation, Diamond>(
                $"diamonds/{diamond.DiamondId}/rough", roughOperation);
            return diamondResult;
        }

        
        // Split Operation
        public async Task<Diamond> SplitDiamondAsync(string roughDiamondId, DateTime participantTimestamp)
        {
            var splitOperation = new SplitOperation()
            {
                ParticipantTimestamp = participantTimestamp
            };
            
            var diamondResult = await _client.PostObjectAsync<SplitOperation,Diamond>(
                $"/diamonds/{roughDiamondId}/split", splitOperation);

            return diamondResult;
        }
        
        // Polished Operations

        public async Task<Diamond> PolishDiamondAsync(string roughDiamondId, Diamond diamond)
        {
            var polishedOperation = _mapper.Map<Diamond, PolishedOperation>(diamond);

            var diamondResult = await _client.PostObjectAsync<PolishedOperation, Diamond>(
                $"diamonds/{roughDiamondId}/polish", polishedOperation);
            return diamondResult;
        }

        public async Task<Diamond> UpdatePolishedDiamondAsync(Diamond diamond)
        {
            var polishedOperation = _mapper.Map<Diamond, PolishedOperation>(diamond);

            var diamondResult = await _client.PatchObjectAsync<PolishedOperation, Diamond>(
                $"diamonds/{diamond.DiamondId}/polished", polishedOperation);
            return diamondResult;
        }

        // Unlock Operation
        public async Task<UnlockDiamondResponse> UnlockDiamondAsync(List<UnlockDiamondRequestItem> diamondsToUnlock)
        {
            UnlockDiamondRequest request = new UnlockDiamondRequest()
            {
                Diamonds = diamondsToUnlock
            };

            var unlockDiamondResponse = await _client.PostObjectAsync<UnlockDiamondRequest, UnlockDiamondResponse>(
                $"bulk/diamonds/unlock", request);
            return unlockDiamondResponse;
        }

        // Delete Operation
        public async Task<DeleteDiamondResponse> DeleteDiamondAsync(List<DeleteDiamondRequestItem> diamondsToDelete)
        {
            DeleteDiamondRequest request = new DeleteDiamondRequest()
            {
                Diamonds = diamondsToDelete
            };

            var deleteDiamondResponse = await _client.PostObjectAsync<DeleteDiamondRequest, DeleteDiamondResponse>(
                $"bulk/diamonds/delete", request);
            return deleteDiamondResponse;
        }
        
        // Inventory
        public async Task<InventoryQueryResults> GetDiamondsInventoryAsync(
            InventoryQuery query, int pageNumber=1, int pageSize=10 )
        {
            var parameters = new List<string>();

            parameters.Add($"page={pageNumber}");
            parameters.Add($"size={pageSize}");

            if (query.LifecycleStates != null)
            {
                foreach (var lifecycleState in query.LifecycleStates)
                {
                    parameters.Add($"lifecycle_state={lifecycleState.ToString().ToLower()}");
                }
            }
            
            if (query.PossessionStates != null)
            {
                foreach (var possessionState in query.PossessionStates)
                {
                    parameters.Add($"possession_state={possessionState.ToString().ToLower()}");
                }
            }
            
            if (query.DiamondIds != null)
            {
                foreach(var diamondId in query.DiamondIds)
                {
                    parameters.Add($"diamond_id={diamondId}");
                }
            }

            if (query.ParticipantIds != null)
            {
                foreach (var participantId in query.ParticipantIds)
                {
                    parameters.Add($"participant_id={participantId}");
                }
            }

            var url = parameters.Count == 0 ? "/diamonds/" : $"/diamonds/?{String.Join("&", parameters)}";

            return await _client.GetObjectAsync<InventoryQueryResults>(url);
        }

        public async Task<Diamond> GetDiamondAsync(string diamondId)
        {
            var diamond = await _client.GetObjectAsync<Diamond>($"/diamonds/{diamondId}");
            return diamond;
        }

        //[+] Shipment and Transfer
        public async Task<InboundShipment> GetInboundShipmentAsync(string shipmentId)
        {
            var shipment = await _client.GetObjectAsync<InboundShipment>($"/shipments/inbound/{shipmentId}");
            return shipment;
        }

        public async Task<List<InboundShipment>> GetAllInboundShipmentsAsync()
        {
            var shipments = await _client.GetObjectAsync<AllInboundShipments>($"/shipments/inbound");
            return shipments.Items;
        }

        public async Task<OutboundShipment> GetOutboundShipmentAsync(string shipmentId)
        {
            var shipments = await _client.GetObjectAsync<OutboundShipment>($"/shipments/outbound/{shipmentId}");
            return shipments;
        }

        public async Task<List<OutboundShipment>> GetAllOutboundShipmentsAsync()
        {
            var shipments = await _client.GetObjectAsync<AllOutboundShipments>($"/shipments/outbound");
            return shipments.Items;
        }
        //[-]

        //[+] Transfer
        public async Task<List<TransferInAcceptResponseItem>> TransferInAcceptAsync(string shipmentId, List<TransferInAcceptItem> diamonds)
        {
            var transferResult = await _client.PostObjectAsync<List<TransferInAcceptItem>, List<TransferInAcceptResponseItem>>(
                $"transfer/in/{shipmentId}/accept", diamonds);
            
            return transferResult;
        }

        public async Task<List<TransferInRejectResponseItem>> TransferInRejectAsync(string shipmentId, List<TransferInRejectItem> diamondIds)
        {
            var transferResult = await _client.PostObjectAsync<List<TransferInRejectItem>, List<TransferInRejectResponseItem>>(
                $"transfer/in/{shipmentId}/reject", diamondIds);
            
            return transferResult;
        }

        public async Task<TransferOutStartResponse> TransferOutStartAsync(string shipmentName, string receiver, List<string> diamondIds)
        {
            var transferOutStartItems = new List<TransferOutStartItem>();
            for(int i = 0; i < diamondIds.Count;i++)
            {
                transferOutStartItems.Add(new TransferOutStartItem {DiamondId = diamondIds[i].ToString()});
            }

            var transferOutStartOperation = new TransferOutStartOperation()
            {
                Diamonds = transferOutStartItems,
                ShipmentName=shipmentName,
                Receiver = receiver
            };

            var transferResult = await _client.PostObjectAsync<TransferOutStartOperation, TransferOutStartResponse>(
                $"transfer/out/start", transferOutStartOperation);
            
            return transferResult;
        }

        public async Task<List<TransferOutCancelResponseItem>> TransferOutCancelAsync(string shipmentId, List<TransferOutCancelItem> diamondIds)
        {
            var transferResult = await _client.PostObjectAsync<List<TransferOutCancelItem>, List<TransferOutCancelResponseItem>> (
                $"transfer/out/{shipmentId}/cancel", diamondIds);
            
            return transferResult;
        }

        //[-]

        // File Management
        public async Task AppendScanFileAsync(
            string diamondId, string filePath, 
            string description="", string scanDeviceManufacturer="",
            string scanDeviceModel="", string scanSoftwareName="", 
            string scanSoftwareVersion="")
        {
            var url = $"diamonds/{diamondId}/files/scan";
            
            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(description), "description"},
                {new StringContent(scanDeviceManufacturer), "scan_device_manufacturer"},
                {new StringContent(scanDeviceModel), "scan_device_model"}
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }

        public async Task AppendScanReportFileAsync(
            string diamondId, string filePath, 
            string description="", string scanDeviceManufacturer="",
            string scanDeviceModel="", string scanSoftwareName="", 
            string scanSoftwareVersion="")
        {
            var url = $"diamonds/{diamondId}/files/scan/reports";
            
            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(description), "description"},
                {new StringContent(scanDeviceManufacturer), "scan_device_manufacturer"},
                {new StringContent(scanDeviceModel), "scan_device_model"},
                {new StringContent(scanSoftwareName), "scan_software_name"},
                {new StringContent(scanSoftwareVersion), "scan_software_version"},
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }

        public async Task AppendPlanFileAsync(
            string diamondId, string filePath, 
            string description="", string scanDeviceManufacturer="",
            string scanDeviceModel="", string planSoftwareManufacturer="", 
            string planSoftwareName="", string planSoftwareVersion="")
        {
            var url = $"diamonds/{diamondId}/files/plan";
            
            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(description), "description"},
                {new StringContent(planSoftwareManufacturer), "plan_software_manufacturer"},
                {new StringContent(planSoftwareName), "plan_software_name"},
                {new StringContent(planSoftwareVersion), "plan_software_version"}
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }

        public async Task AppendPlanReportAsync(
            string diamondId, string filePath, 
            string description="", string scanDeviceManufacturer="",
            string scanDeviceModel="", string planSoftwareManufacturer="", 
            string planSoftwareName="", string planSoftwareVersion="")
        {
            var url = $"diamonds/{diamondId}/files/plan/reports";
            
            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(description), "description"},
                {new StringContent(scanDeviceManufacturer), "scan_device_manufacturer"},
                {new StringContent(scanDeviceModel), "scan_device_model"},
                {new StringContent(planSoftwareManufacturer), "plan_software_manufacturer"},
                {new StringContent(planSoftwareName), "plan_software_name"},
                {new StringContent(planSoftwareVersion), "plan_software_version"}
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }
        
        public async Task AppendImageFileAsync(string diamondId, 
            string filePath, 
            string description, 
            ImageTypeEnum imageType)
        {
            var url = $"diamonds/{diamondId}/files/images";
            
            var imageTypeValue = JsonConvert.SerializeObject(imageType, Converter.Settings).ToLower();
            imageTypeValue = imageTypeValue.Replace("\"", "");
            
            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(description), "description"},
                {new StringContent(imageTypeValue), "image_type"}
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }
        
        public async Task AppendVideoFileAsync(string diamondId, 
            string filePath, 
            string description)
        {
            var url = $"diamonds/{diamondId}/files/videos";

            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(description), "description"}
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }
        
        public async Task AppendSupplementaryFileAsync(string diamondId, 
            string filePath, 
            string description)
        {
            var url = $"diamonds/{diamondId}/files/supplementary_files";

            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(description), "description"}
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }

        public async Task AppendGradingCertificateAsync(
            string diamondId, string filePath, 
            string gradingLab, DateTime gradedDate,
            string graderCertificateNo, string graderReportNo="", 
            string graderInscriptionNo="")
        {
            var url = $"diamonds/{diamondId}/files/grading_certificates";

            // Generate multipart content for the upload
            var content = new MultipartFormDataContent
            {
                {new StringContent(gradingLab), "grading_lab"},
                {new StringContent(gradedDate.ToString("s")), "graded_date"},
                {new StringContent(graderCertificateNo), "grader_certificate_no"},
                {new StringContent(graderReportNo), "grader_report_no"},
                {new StringContent(graderInscriptionNo), "grader_inscription_no"},
            };
            
            var fileHandler = File.OpenRead(filePath);
            var streamContent = new StreamContent(fileHandler);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + Path.GetFileName(filePath) + "\"");
            content.Add(streamContent, "file", Path.GetFileName(filePath));

            await _client.PostFileAsync(url, content);
        }

        public async Task DownloadFileAsync(string diamondId, string hash, string filePath)
        {
            var url = $"/diamonds/{diamondId}/files/{hash}";
            
            //Create folder path if required
            Directory.CreateDirectory(Path.GetDirectoryName(filePath) ?? throw new ArgumentException());

            //Get file content
            using var contentStream = await _client.GetStreamAsync(url);

            //Copy to the file
            using var fileStream = new FileStream(filePath,FileMode.Create);
            await contentStream.CopyToAsync(fileStream);
        }

        public void Dispose() {
            _client.Dispose();
        }
    }
}
